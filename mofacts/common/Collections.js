/* Collections - our data collections stored in MongoDB
 * */

Stimuli = new Meteor.Collection("stimuli");
Tdfs = new Meteor.Collection("tdfs");
UserTimesLog = new Meteor.Collection("userTimesLog");
UserProfileData = new Mongo.Collection("userProfileData");
UserMetrics = new Mongo.Collection("userMetrics");
ScheduledTurkMessages = new Mongo.Collection("scheduledTurkMessages");

//Fernando's Collection
Gamification = new Mongo.Collection("GamificationModel");
Scores = new Mongo.Collection("GamificationScores");
AchievementModels = new Mongo.Collection("AchievementModels");
Achievements = new Mongo.Collection("Achievement");
UserSessions = new Mongo.Collection("UserSessions");
ExperimentData = new Mongo.Collection("ExperimentData");