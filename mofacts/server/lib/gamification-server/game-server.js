createReferenceModels = function () {
    serverConsole("Verifying Gamificantion Reference Models");
    Meteor.call('debugLog', Meteor.myFunctions.createGamificationReferenceModels());
    Meteor.call('debugLog', Meteor.myFunctions.createAchivementModels());
    if (Meteor.settings.isGamificationExperiment) {
        Meteor.call('debugLog', Meteor.myFunctions.createExperimentControl());
    }


};
/* Methods */
Meteor.myFunctions = {
    // User Gamification Reference model
    createExperimentControl: function () {
        var obj = {};
        if (ExperimentData.find().count() > 0) {

            obj = ExperimentData.find({}).fetch()[0];
            obj.updateServer.push(new Date());
            ExperimentData.upsert(obj._id, obj);
            return null;
        }

        obj.StartExperiment = new Date();
        obj.updateServer = [new Date()];
        obj.perfilAchiever = 0;
        obj.perfilSocializer = 0;
        obj.perfilDynamic = 0;
        obj.perfilImersion = 0;
        obj.perfilstandard = 0;
        obj.microGamification = 0;
        obj.macroGamification = 0;
        ExperimentData.upsert(obj._id, obj);
        return "Created Experiment Control";
    },
    createGamificationReferenceModels: function () {
        if (!Meteor.settings.public.UseGamification) {
            return null;
        }

        if ((!Meteor.settings.UseGamificationModelDynamic) || (Meteor.settings.isGamificationExperiment)) {
            Meteor.call('debugLog', "entrou na função createGamificationReferenceModels");
            //if (Gamification.findOne({"name": "Modelo-achievement"}) == null) {
            var achievementModelo = {
                name: "Modelo-achievement",
                leaderboard: true,
                personalStatus: false,
                hasOption: true,
                showScore: true,
                score: true,
                gameOn: false, 
                background: false,
                achievementBoard: true,
                showHealth: true,
                Health: 100,
                MaxHealth: 100,
                showCallChalange: true,
                escaleteChallange: true,
                "scoreRules": {
                    level: true,
                    ratio: 0.2,
                    dynamicLeaderboard: true,
                    levelDificulty: 0.3,
                    recoverHealthValue: 10,
                    recoverHealthType: "%",
                    damageValue: 10,
                    damageOnTimeOut: 5,
                    damageType: "%"
                }
            };
            var socialModelo = {name: "Modelo-social",
                showHealth: true,
                Health: 100,
                MaxHealth: 100,
                profileOn: false,
                gameOn: true,                
                showScore: true,
                score: true,
                background: false,
                achievementBoard: true,
                showCallChalange: true,
                escaleteChallange: true,
                "scoreRules": {level: true,
                    ratio: 0.5,
                    levelDificulty: 0.9,
                    gameChance: 0.5,
                    recoverHealthValue: 10,
                    recoverHealthType: "%",
                    damageValue: 10,
                    damageOnTimeOut: 5,
                    damageType: "%"}};
            var imersionModelo = {name: "Modelo-imersion",
                showHealth: true,
                Health: 100,
                MaxHealth: 100,
                leaderboard: false,
                personalStatus: false,
                hasOption: false,
                profileOn: false,
                gameOn: false,
                avatar: true,
                background: true,
                achievementBoard: true,
                showScore: true,
                score: true,
                showCallChalange: true,
                escaleteChallange: true,
                "scoreRules": {recoverHealthValue: 10,
                    recoverHealthType: "%",
                    damageValue: 10,
                    damageOnTimeOut: 5,
                    damageType: "%"
                }
            };
            var standardModel = {
                name: "Standard-Model",
                showHealth: true,
                Health: 100,
                MaxHealth: 100,
                showCallChalange: true,
                leaderboard: true,
                hasOption: true,
                showScore: true,
                score: true,
                gameOn: true,
                avatar: true,
                background: true,
                escaleteChallange: true,
                "scoreRules": {level: true, ratio: 0.5, levelDificulty: 0.9,
                    gameChance: 0.5, recoverHealthValue: 10,
                    recoverHealthType: "%",
                    damageValue: 10,
                    damageOnTimeOut: 5,
                    damageType: "%"}};
            Meteor.call('connGamification', achievementModelo.name, achievementModelo, "upsert");
            Meteor.call('connGamification', socialModelo.name, socialModelo, "upsert");
            Meteor.call('connGamification', imersionModelo.name, imersionModelo, "upsert");
            Meteor.call('connGamification', standardModel.name, standardModel, "upsert");
            //    return ("inserted");
            //}
        }
        return (" Gamification reference models are up to date");
    },
    esterothypedGamification: function (profile, force) {
        var obj = {};
        var especifyProfile = profile["majoritario"];
        if (force !== null)
            especifyProfile = force;
        switch (especifyProfile) {
            case "Realizacao" :
                obj = Gamification.findOne({"name": "Modelo-achievement"});
                obj.name = "Achiever";
                break;
            case "Social"    :
                obj = Gamification.findOne({"name": "Modelo-social"});
                obj.name = "Socializer";
                break;
            case "Imersao"    :
                obj = Gamification.findOne({"name": "Modelo-imersion"});
                obj.name = "Imersive";
                break;
            default :
                obj = Gamification.findOne({"name": "Standard-Model"});
                obj.name = "Standard";
                break;
        }
        return obj;
    },
    dynamicGamification: function (profile) {
        console.log("cheguei em dynamic");
        /*    //  [{"id": "100033",
         //                    "grupo": "CAEDLAB2",
         //                    "nome": "fernando",
         //                    "idade": "12",
         //                    "email": "teste@gmail.com",
         //                    "genero": "m", "escolaridade":
         //                            "Ensino Fundamental Incompleto",
         //                    "realizacao": "2.0000",
         //                    "social": "-2.0000",
         //                    "imersao": "-1.2000",
         //                    "majoritario": "Realizacao",
         //                    "mecanica": "0",
         //                    "competicao": "2.0000",
         //                    "avanco": "2.0000",
         //                    "relacionamento": "-2.0000",
         //                    "trabalhoemequipe": "-2.0000",
         //                    "roleplaying": "0.0000",
         //                    "customizacao": "-1.5000",
         //                    "descoberta": "0",
         //                    "escapismo": "0",
         //                    "socializacao": "-2.0000",
         //                    "dataentrada": "2017-02-06 12:48:35"}] */

        var obj = {};
        switch (profile["majoritario"]) {
            case "Realizacao" :
                obj.name = "Achiever";
                break;
            case "Social"    :
                obj.name = "Socializer";
                break;
            case "Imersao"    :
                obj.name = "Imersive";
                break;
            default :
                obj.name = "Standard";
                break;
        }
        //Empty Model
        obj.dynamic = true;    
        obj.changeParams = false;
        obj.leaderboard = false;
        obj.achievementBoard = false;
        obj.personalStatus = false;
        obj.showCallChalange = false;
        obj.hasOption = false;
        obj.gameOn = false;
        obj.avatar = false;
        obj.background = false;
        obj.showScore = false;
        obj.scoreRules = {};
        obj.scoreRules.level = false;
        obj.score = true;
        obj.showHealth = true;
        obj.escaleteChallange = true;
        obj.Health = 100;
        obj.MaxHealth = 100;
        obj.scoreRules.ratio = 0.2;
        obj.scoreRules.gameChance = 0.5;
        obj.scoreRules.dynamicLeaderboard = true;
        obj.scoreRules.levelDificulty = 0.3;
        obj.scoreRules.recoverHealthValue = 10;
        obj.scoreRules.recoverHealthType = "%";
        obj.scoreRules.damageValue = 10;
        obj.scoreRules.damageOnTimeOut = 5;
        obj.scoreRules.damageType = "%";
        ////////////////////////////////////////////

        if (profile["mecanica"] > 1)
            obj.changeParams = true;
//                if (profile["relacionamento"] > 1)
//                    if (profile["descoberta"] > 1)
//                        if (profile["escapismo"] > 1)
        if ((profile["competicao"] > 1)) {
            obj.leaderboard = true;
        }
        if ((profile["socializacao"] > 1) || (profile["competicao"] > 1)) {
            obj.showCallChalange = true;
            obj.showScore = true;
            obj.scoreRules.level = true;
        }

        if (profile["avanco"] > 1) {
            obj.achievementBoard = true;
            obj.personalStatus = true;
            obj.showScore = true;
            obj.scoreRules.level = true;
        }

        if (obj.personalStatus && obj.leaderboard)
            obj.hasOption = true;
        if (profile["customizacao"] > 1) {
         //   obj.profileOn = true;
        }
        if (profile["roleplaying"] > 1) {
            obj.avatar = true;
            obj.background = true;
        }
        if ((profile["trabalhoemequipe"] > 1)) {
            obj.gameOn = true;
            obj.showScore = true;
            obj.scoreRules.level = true;
        }

        return obj;
    },
    createAchivementModels: function () {
        if (!Meteor.settings.public.UseGamification) {
            return null;
        }
        Meteor.call('debugLog', "entrou na função createAchievementModels ");
        var model = [{}];
        var tdfname = "HiraganaOptim_xml";
        var LessonName = "Pratica de Hiragana e Katakana";
        model[0] = {tdf: tdfname, lessonname: LessonName,
            unlocked: false,
            title: "Aquecendo",
            description: "Faça 50 pontos pra começar a aquecer",
            variable: "points",
            operator: "=",
            value: "50",
            icoUrl: null,
            createdBy: "GameMaster"
        };
        model.push({tdf: tdfname, lessonname: LessonName,
            unlocked: false,
            title: "Profissional",
            description: "Faça 300 pontos",
            variable: "points",
            operator: "=",
            value: "300",
            icoUrl: null,
            createdBy: "GameMaster"

        });
        model.push({tdf: tdfname, lessonname: LessonName,
            unlocked: false,
            title: "Mestre",
            description: "Responda 100 cartões corretamente",
            variable: "acertos",
            operator: "=",
            value: "100",
            icoUrl: null,
            createdBy: "GameMaster"
        });
        model.push({tdf: tdfname, lessonname: LessonName,
            unlocked: false,
            title: "Grande Mestre",
            description: "Faça 1000 pontos",
            variable: "points",
            operator: "=",
            value: "1000",
            icoUrl: null,
            createdBy: "GameMaster"
        });
        var checked = false;
        for (var i = 0; i < model.length; i++) {
            var exists = AchievementModels.findOne(model[i]);
            if (exists == null) {
                //Meteor.call('connAchievementModels',model[i].title, model[i], "upsert");
                //console.log(model[i]);
                Meteor.call('connAchievementModels', null, model[i], "insert");
                checked = true;
            }
        }
        ;
        if (!checked)
            return (" Achievement reference models are up to date");
        else
            return ("updated reference model");
    },
    verificaModeloUsuario: function (isTest, user) { //check User Profile            
        if (!Meteor.settings.public.UseGamification) {
            return null;
        }
        Meteor.call('debugLog', "entrou na função verificaModeloUsuario");
        Meteor.call('debugLog', "verificando usuario: " + user._id.toString());
        var userModel = Gamification.findOne({_id: user._id});
        /* for (var property in userModel) {
         Meteor.call('debugLog', " " + property.toString());
         }*/
        if (!userModel == null) { //fernando ==null ?? ta certo  isso
            Meteor.call('debugLog', "Modelo de gamificação Encontrado !== null");
            var userScores = Scores.findOne({_id: user._id});
            if (userScores == null) {
                Meteor.call('debugLog', "createUserScoresModel == null ");
                /*
                 //                Meteor.call('createUserScoresModel', user, function (error, result) {
                 //                    if (error) {
                 //                        Meteor.call('debugLog', "erro chamada da createUserScoresModel");
                 //                        return(error);
                 //                    } else {
                 //                        Meteor.call('debugLog', "result chamada da createUserScoresModel");
                 //                        return(result);
                 //                    }
                 //                });
                 */
                Meteor.call('debugLog', Meteor.myFunctions.createUserScoresModel(user));
            } else {
                Meteor.call('debugLog', "userScore !== null usuario:");
            }
            return userModel;
        } else {
            Meteor.call('debugLog', "Modelo de gamificação não encontrado " + user._id);
            /* 
             
             //                 Meteor.call('verificaRespostaPerfil', isTest, user, function (error, result) {
             //                    if (error) {
             //                        Meteor.call('debugLog', "erro chamada da verificaRespostaPerfil");
             //                        return(error);
             //                    } else {
             //                        Meteor.call('debugLog', "result chamada da verificaRespostaPerfil");
             //                        return(result);
             //                    }
             //                });
             */
            return Meteor.myFunctions.verificaRespostaPerfil(isTest, user);
        }

        //return "entrou";
    },
    verificaRespostaPerfil: function (isTest, user) {
        Meteor.call('debugLog', "entrou na função verificaRespostaPerfil");
        //var varUserTarget = user.username;
        var varUserTarget = "";
        if (isTest) {
            Meteor.call('debugLog', "isTest " + user.username);
            varUserTarget = user.username;
        } else {
            Meteor.call('debugLog', "not isTest " + user.username);
            varUserTarget = user.emails[0].address;
            //varUserTarget = user.username;            
        }
        Meteor.call('debugLog', "target = " + varUserTarget);
        var url = 'http://acessopesquisa.caed-lab.com/gpmAPI.php'; //API address
        //     var url = 'http://jsonplaceholder.typicode.com/posts';
        //'http://localhost/git/GrupoDePesquisaManager/gpmAPI.php';
        var varAction = "mofacts"; //Action recquired from API
        var varSigla = "CAEDLAB"; //ID
        var varPassword = "123lab"; //Password
        var varGroupCode = "CAEDLAB2"; //User group -- AllCAEDGRP
        var varDatabaseName = "ambos"; //Database name: qpjbr  - experimental - ambos
        //http://acessopesquisa.caed-lab.com/gpmAPI.php?action=mofacts&sigla=CAEDLAB&senha=123lab&usuarioAlvo=teste@gmail.com&codgrp=CAEDLAB2&db=ambos
        //http://acessopesquisa.caed-lab.com/gpmAPI.php?action=exportaUsuario&sigla=CAEDLAB&senha=123lab&usuarioAlvo=teste@gmail.com&codgrp=CAEDLAB2&db=qpjbr
        //http:localhost/git/GrupoDePesquisaManager/gpmApi.php?action=exportaUsuario&sigla=CAEDLAB&senha=123lab&usuarioAlvo=fernando.heb@gmail.com&codgrp=CAEDLAB1
        //http:localhost/git/GrupoDePesquisaManager/gpmAPI.php?action = 'exportaUsuario' & sigla = 'CAEDLAB' & senha = '123lab' & codgrp = 'CAEDLAB1' & bd= ''&usuarioAlvo=fernando.heb@gmail.com&
        //var vurl = url + Vaction + Vsigla + Vsenha +VusuarioAlvo+ Vcodgrp;
        //console.log(vurl);
        //  console.log("meteor Call bloco try");
        var result = Meteor.http.call('GET', url, {
            // http:acessopesquisa.caed-lab.com/gpmAPI.php?
            params: {
                action: varAction,
                sigla: varSigla,
                senha: varPassword,
                usuarioAlvo: varUserTarget,
                codgrp: varGroupCode,
                db: varDatabaseName
            },
            //headers:{
            // application/jsonrequest
            // }
            //
        }, function (error, result) {
            if (!error) {
                //         console.log("callback docallback");
                //         console.log(result);
                var JsonRes = JSON.parse(result.content)[0];
                //         console.log(JsonRes);
                //
                //show the json result
                //for (var property in JsonRes) {
                //    Meteor.call('debugLog', " " + property.toString());
                //}

                if (!JsonRes.hasOwnProperty("erro")) {


//                        return  Meteor.call('createUserGamificationModel', user, JsonRes, function (error, result) {
//                            if (error) {
//                                // handle error
//                                Meteor.call('debugLog', "erro na Criação dos modelos do usuario");
//                            } else {
//                                Meteor.call('debugLog', "ok na Criação do modelo do usuario");
//                                return(result);
//                            }
//                        }); //trocar por função callback

                    return Meteor.myFunctions.createUserGamificationModel(user, JsonRes);
                } else {
                    Meteor.call('debugLog', "erro no json " + JsonRes.msg);
                    return (console.log(JsonRes.erro + "  " + JsonRes.msg));
                }
            } else {
                Meteor.call('debugLog', "erro na requisição HTTP ");
                return console.log(error);
            }
        });
    },
//    Require call from client side
//     
//     callCreateUserAchivementModel: function (user, tdfName) {
//         Meteor.call('debugLog',Meteor.myFunctions.createUserAchievementModel(user,tdfName));
//     },
    createUserGamificationModel: function (user, profile) {
        Meteor.call('debugLog', "entrou na função createUserGamificationModel");
        console.log(profile["majoritario"]);
        console.log(user);
        var obj = {};
        console.log(obj);
        console.log("isGamificationExperiment = " + Meteor.settings.isGamificationExperiment);
        if (Meteor.settings.isGamificationExperiment) {
            obj = Meteor.myFunctions.experimentGamification(profile);
        } else {
            console.log("UseGamificationModelDynamic = " + Meteor.settings.UseGamificationModelDynamic);
            if (!Meteor.settings.UseGamificationModelDynamic) {
                obj = Meteor.myFunctions.esterothypedGamification(profile, null);
            } else {
                obj = Meteor.myFunctions.dynamicGamification(profile);
            }
        }
        console.log(obj);
        obj["_id"] = user._id;
        obj["ExtProfile"] = profile;
        Meteor.call('connGamification', obj._id, obj, "upsert");
        console.log(obj);
        Meteor.myFunctions.createUserScoresModel(user);
//        Meteor.call('createUserScoresModel', user, function (error, result) {
//            if (error) {
//                Meteor.call('debugLog', "erro chamada da createUserScoresModel");
//                return(error);
//            } else {
//                Meteor.call('debugLog', "result chamada da createUserScoresModel");
//                return(result);
//            }
//        });
        return Gamification.findOne(obj.id);
    },
    experimentGamification: function (profile) {
        console.log("Experinment. count " + ExperimentData.find().count());
        if (ExperimentData.find().count() > 0) {
            var obj = {};
            var updateSet = {};
            var expData = ExperimentData.find().fetch()[0];
            console.log("Experiment method " + Meteor.settings.ExperimentMethod);
            console.log("Majoritário " + profile["majoritario"]);
            console.log(expData);
            console.log("Dynamic? " + Meteor.settings.UseGamificationModelDynamic);
            switch (Meteor.settings.ExperimentMethod) {
                case "balanceado" :
                    if (!Meteor.settings.UseGamificationModelDynamic) {
                        switch (profile["majoritario"]) {
                            case "Realizacao" :
                                if (expData.perfilAchiever <= expData.perfilstandard) {
                                    updateSet = {$inc: {perfilstandard: 1}};
                                    obj = Meteor.myFunctions.esterothypedGamification(profile, "Genérico");
                                } else {
                                    updateSet = {$inc: {perfilAchiever: 1}};
                                    obj = Meteor.myFunctions.esterothypedGamification(profile, null);
                                }
                                break;
                            case "Social"    :
                                if (expData.perfilSocializer <= expData.perfilstandard) {
                                    updateSet = {$inc: {perfilstandard: 1}};
                                    obj = Meteor.myFunctions.esterothypedGamification(profile, "Genérico");
                                } else {
                                    updateSet = {$inc: {perfilSocializer: 1}};
                                    obj = Meteor.myFunctions.esterothypedGamification(profile, null);
                                }
                                break;
                            case "Imersao"    :
                                if (expData.perfilImersion <= expData.perfilstandard) {
                                    updateSet = {$inc: {perfilstandard: 1}};
                                    obj = Meteor.myFunctions.esterothypedGamification(profile, "Genérico");
                                } else {
                                    updateSet = {$inc: {perfilImersion: 1}};
                                    obj = Meteor.myFunctions.esterothypedGamification(profile, null);
                                }
                                break;
                            default :
                                updateSet = {$inc: {perfilstandard: 1}};
                                //ExperimentData.update(expData._id, updateSet);
                                obj = esterothypedGamification(profile, "Genérico");
                                break;
                        }
                    } else {
                        console.log("expData.perfilDynamic  " + expData.perfilDynamic);
                        console.log("expData.perfilstandard  " + expData.perfilstandard);
                        if (expData.perfilDynamic <= expData.perfilstandard) {
                            updateSet = {"$inc": {"perfilDynamic": 1}};
                            console.log(updateSet);
                            obj = Meteor.myFunctions.dynamicGamification(profile);
                        } else {
                            updateSet = {"$inc": {"perfilstandard": 1}};
                            console.log(updateSet);
                            obj = Meteor.myFunctions.esterothypedGamification(profile, "Genérico");
                        }

                    }
                    break;
                case "aleatorio" :
                    if (!Meteor.settings.UseGamificationModelDynamic) {
                        var Perfis = ["Realizacao", "Social", "Genérico", "Imersao"];
                        var diceFace = RollDice(4) - 1;
                        switch (Perfis[diceFace]) {
                            case "Realizacao" :
                                updateSet = {$inc: {perfilAchiever: 1}};
                                break;
                            case "Social" :
                                updateSet = {$inc: {perfilSocializer: 1}};
                                break;
                            case "Imersao"    :
                                updateSet = {$inc: {perfilImersion: 1}};
                                break;
                            default :
                                updateSet = {$inc: {perfilstandard: 1}};
                                break;
                        }
                        obj = Meteor.myFunctions.esterothypedGamification(profile, Perfis[diceFace]);
                    } else {
                        var diceFace = RollDice(2);
                        console.log("dadoface " + diceFace);
                        if (diceFace == 1) {
                            updateSet = {$inc: {perfilDynamic: 1}};
                            obj = Meteor.myFunctions.dynamicGamification(profile);
                        } else {
                            updateSet = {$inc: {perfilstandard: 1}};
                            obj = Meteor.myFunctions.esterothypedGamification(profile, "Genérico");
                        }
                    }
                    break;
                case "BalanceadoMicroVsMacro" :
                    var lowest;
                    if (expData.macroGamification <= expData.microGamification) {
                        lowest = "macrogamification";
                        if (expData.macroGamification > expData.perfilstandard)
                            lowest = "perfilstandard";
                    } else {
                        lowest = "microgamification";
                        if (expData.microGamification > expData.perfilstandard)
                            lowest = "perfilstandard";
                    }
                    switch (lowest) {
                        case "macrogamification":
                            switch (profile["majoritario"]) {
                                case "Realizacao" :
                                    updateSet = {$inc: {perfilAchiever: 1, macroGamification: 1}};
                                    break;
                                case "Social" :
                                    updateSet = {$inc: {perfilSocializer: 1, macroGamification: 1}};
                                    break;
                                case "Imersao"    :
                                    updateSet = {$inc: {perfilImersion: 1, macroGamification: 1}};
                                    break;
                                default :
                                    updateSet = {$inc: {perfilstandard: 1}};
                                    break;
                            }
                            obj = Meteor.myFunctions.esterothypedGamification(profile, null);
                            break;
                        case "microgamification":
                            updateSet = {$inc: {perfilDynamic: 1, microGamification: 1}};
                            obj = Meteor.myFunctions.dynamicGamification(profile);
                            break;
                        case "perfilstandard":
                            updateSet = {$inc: {perfilstandard: 1}};
                            obj = Meteor.myFunctions.esterothypedGamification(profile, "Genérico");
                            break;
                    }
            }


            console.log("final" + obj.name);
            ExperimentData.update(expData._id, updateSet);
            return obj;
            
        }
    },
    createUserScoresModel: function (user) {
        Meteor.call('debugLog', "entrou na função createUserScoresModel");
        //console.log(user);
        var obj = {};
        //console.log(obj);
        obj["_id"] = user._id;
        obj["totalLevel"] = 0;
        obj["totalPoints"] = 0;
        Meteor.call('connScores', obj._id, obj, "upsert");
        //console.log(obj);
        return Scores.findOne(obj.id);
    },
    createUserAchievementModel: function (user, tdfName) {
        var tdfname = buildTdfDBName(tdfName);
        Meteor.call('debugLog', "entrou na função createUserAchievementModel user:" + user._id.toString() + " tdf: " + tdfname.toString());
        var query = {};
        query.user_id = user._id;
        query.tdf = tdfname.toString();
        var userAchievements = Achievements.find(query);
        if (!userAchievements) {
            userAchievements = [{}];
            Meteor.call('debugLog', "tdf vazio");
        }
        var queryModel = {tdf: tdfname.toString()};
        Meteor.call('debugLog', userAchievements.toString());
        AchievementModels.find(queryModel).forEach(function (modelItem) {
            var notfound = true;
            userAchievements.forEach(function (userItem) {
                Meteor.call('debugLog',
                        " Procurando: createdBy " + modelItem.createdBy + " " + userItem.createdBy
                        + "title " +
                        modelItem.title + " " + userItem.title
                        + "description " +
                        modelItem.description + " " + userItem.description
                        + "variable " +
                        modelItem.variable + " " + userItem.variable
                        + "achievement_id " + userItem.Achievment_id + " " + modelItem._id);
                if (userItem.Achievment_id == modelItem._id) {
                    notfound = false;
                    Meteor.call('debugLog', "found true encontrou um igual");
                }
            });
            if (notfound) {
                /*var achievement = {'$push': {}};
                 //var push = {'$push':{}};
                 /*  action = [{'$push': {}, '$inc': {}}];
                 action[0]['$push'][makeKey(idx, 'answerTimes')] = answerTime;
                 db.students.update(
                 { name: "joe" },
                 { $push: { scores: { $each: [ 90, 92, 85 ] } } }
                 )
                 //achievement._id = user._id;                    
                 achievement['$push'][tdfname] = item;*/
                Meteor.call('debugLog', "notfound false, então devia estar criando");
                modelItem.user_id = user._id;
                modelItem.unlocked = false;
                modelItem.Achievment_id = modelItem._id;
                delete modelItem._id;
                Meteor.call("connAchievements", null, modelItem, "insert");
                notifyAchievmentUnlocked(user, "Model Created");
            }
        });
        return ("Updated Achievements for user " + user._id + " and tdf " + tdfname);
//        } else {
//            return ("Achievements for user " + user._id + " and tdf " + tdfname + " already exists");
//        }
    },
    ShowTdfQuestionsAnswers: function (tdfName) {
        var obj = [{}];
        var i = 0;
//        something.forEach({ 
//            obj[i].question = x;
//            obj[i].answer = y;
//        });

        return obj;
    },
};
/*****
 if (profile["realizacao"] > 0){
 obj.leaderboard = true;
 obj.personalStatus= false;
 obj.score= true;
 obj.achievementBoard= true;
 obj.scoreRules.level= true; 
 obj.scoreRules.ratio= 0.2;
 obj.scoreRules.levelDificulty =0.3;
 }
 if (profile["social"] > 0) {
 obj.gameOn= true;
 obj.score= true;
 obj.scoreRules.gameChance= 0.75;
 }
 if (profile["imersao"] > 0) {
 obj.profileOn= true;
 obj.avatar= true;
 obj.background= true;
 }
 if (profile["majoritario"] > 0) {
 
 }
 ******/
/*****
 
 
 if (profile["avanco"] > 0) {
 }
 if (profile["relacionamento"] > 0) {
 }
 if (profile["trabalhoemequipe"] > 0) {
 }
 if (profile["roleplaying"] > 0) {
 }
 if (profile["customizacao"] > 0) {
 }
 if (profile["socializacao"] > 0) {
 }
 
 if (profile["descoberta"] > 0) {
 }
 if (profile["escapismo"] > 0) {
 }    
 * @type type      */
