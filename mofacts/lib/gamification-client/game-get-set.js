//////////////////////////////////////////////////////////////////////////
// Helpful utility functions for Meteor users
//Verify if the Gamification Profile Exists and try to Locate the answers and create if its exists in the questionnaire database.


//Fernando Gamification
//Get Gamification models

//var math = require('mathjs');

userGetUsername = function (user) {
    if (!user)
        return null;
    var username;
    Meteor.users.find({_id: user._id, "username": {$exists: true}}).forEach(function (user) {
        username = null;
        username = user.username;
    });
    return username;
};
userGetShortname = function (user) {
    if (!user)
        return null;
    var username = userGetUsername(user);
    if (username == null)
        return null;
    return username.slice(0, username.search("@"));
};
GetUserSession = function (SessionID) {
    if (SessionID)
        return UserSessions.findOne({_id: SessionID});
    else
        return null;
};
//Gets for Scores Controls
GetUserScores = function (user) {
    if (!user)
        return null;
    return Scores.findOne({_id: user._id});
};
GetUserPoints = function (user, tdfName) {
    if (user == null) {
        user = Meteor.user();
    }
    var points = 0;
    var tdfname = buildTdfDBName(tdfName);
    var userScores = GetUserScores(user);
    if (!userScores)
        return points;
    if (userScores.hasOwnProperty(tdfname)) {
        points = userScores[tdfname].points;
    }
    /* var tryValues = getCurrentScoreValues();
     console.log( "try calues "+Values[0]);*/
    return points;
};
GetUserLevel = function (user, tdfName) {
    if (user == null) {
        user = Meteor.user();
    }


    var level = 0;
    if (tdfName == null)
        return level;
    var tdfname = buildTdfDBName(tdfName);
    var userScores = GetUserScores(user);
    if (!userScores)
        return level

    if (userScores.hasOwnProperty(tdfname)) {
        level = userScores[tdfname].level;
    }

    return level;
};
GetUserTotalPoints = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var totalPoints = 0;
    var userScores = GetUserScores(user);
    if (!userScores)
        return totalPoints;
    totalPoints = userScores.totalPoints;
    return totalPoints;
};
GetUserTotalLevel = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var totalLevel = 0;
    var userScores = GetUserScores(user);
    if (!userScores)
        return totalLevel;
    totalLevel = userScores.totalLevel;
    return totalLevel;
};
//gets for Gamification user model
GetUserModel = function (user) {
    if (!Meteor.settings.public.UseGamification) {
        return null;
    }
    if (user == null) {
        user = Meteor.user();
    }
    if (user)
        return Gamification.findOne({_id: user._id});
    else
        return null;
};
GetUserProfileName = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("name")) {
            return userModel.name;
        }
    }
    return null;
};
GetUserExternProfileName = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("ExtProfile")) {
            if (userModel.ExtProfile.hasOwnProperty("majoritario")) {
                return userModel.ExtProfile.majoritario;
            }
        }
    }
    return null;
};
GetUserExternProfileIdade = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("ExtProfile")) {
            if (userModel.ExtProfile.hasOwnProperty("idade")) {
                return userModel.ExtProfile.idade;
            }
        }
    }
    return null;
};
GetUserExternProfileGenero = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("ExtProfile")) {
            if (userModel.ExtProfile.hasOwnProperty("genero")) {
                return userModel.ExtProfile.genero;
            }
        }
    }
    return null;
};
GetUserExternProfileGrupoOrigem = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("ExtProfile")) {
            if (userModel.ExtProfile.hasOwnProperty("grupo")) {
                return userModel.ExtProfile.grupo;
            }
        }
    }
    return null;
};
GetUserExternProfileEscolaridade = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("ExtProfile")) {
            if (userModel.ExtProfile.hasOwnProperty("escolaridade")) {
                return userModel.ExtProfile.escolaridade;
            }
        }
    }
    return null;
};
GetUserIsMicroGamified = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("dynamic")) {
            return true;
        }
    }
    return false;
};

GetExperimentalGroup = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("dynamic")) {
            return "micro";
        }
        switch (GetUserProfileName(user)) {
            case "Achiever"
                    :
                return "macro";
                break;
            case "Socializer"   :

                return "macro";
                break;
            case "Imersive"   :
                return "macro";
                break;
            default :
                return "standard";
                break;
        }

    }
    return false;
}
GetUserAchievements = function (user, tdfname, unlocked) {
    if (user == null) {
        user = Meteor.user();
    }
    var query = {};
    var orderby = {};
    orderby.sort = {};
    orderby.sort.tdf = -1;
    query.user_id = user._id;
    if (tdfname != null) {
        query.tdf = buildTdfDBName(tdfname);
    }
    if (unlocked) {
        query.unlocked = unlocked;
    } else if (unlocked === false) {
        query =
                {$and: [{$or: [{unlocked: false}, {unlocked: {$exists: false}}]}, {user_id: user._id}]};
        //query.unlocked = unlocked;
    }

    if (unlocked) {
        orderby.sort.dateUnlocked = -1;
    }
//  console.log(orderby);
//  console.log(query);
    var userAchievements = Achievements.find(query, orderby).fetch();
    if (userAchievements != null) {
        return userAchievements;
    } else {
        return false;
    }

};
GetUserShowScore = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("showScore")) {
            return userModel.score;
        }
    }
    return false;
};
GetUserDynamicLeaderboard = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);   
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("dynamicLeaderboard")) {
                return  userModel.scoreRules.dynamicLeaderboard;
            }
        }
    }
    return false;
};
GetUserHasOption = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("hasOption")) {
            return userModel.hasOption;
        }
    }
    return false;
};
ExportInteractionLog = function (user) {
    var logs = [];


    logs["leaderboard"] = 0;
    logs["personalStatus"] = 0;
    logs["achievementBoard"] = 0;
    logs["Achievements"] = 0;
    logs["AchievementsCreation"] = 0;
    logs["ExpendedPoints"] = 0;
    logs["gameOn"] = 0;
    logs["GameOn"] = 0;
    logs["hasOption"] = 0;
    logs["levelTotal"] = 0;
    logs["pointsTotal"] = 0;
    logs["showCallChalange"] = 0;
    logs["showHealth"] = 0;
    logs["QuestionText"] = 0;
    logs["AskedHint"] = 0;
    logs["Study"] = 0;
    logs["custo 0"] = 0;
    logs["custo 50"] = 0;
    logs["custo 100"] = 0;
    logs["custo 150"] = 0;
    logs["custo 200"] = 0;
    logs["custo 250"] = 0;
    logs["custo 300"] = 0;
    logs["custo 350"] = 0;
    logs["custo 400"] = 0;
    logs["custo 450"] = 0;
    logs["custo 500"] = 0;
    logs["custo 550"] = 0;
    logs["custo 600"] = 0;
    logs["custo 650"] = 0;
    logs["custo 700"] = 0;
    logs["custo 750"] = 0;
    logs["custo 800"] = 0;
    logs["custo 3000"] = 0;


    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("InteractionLogs")) {
            var count = 0;
            for (var key in userModel.InteractionLogs) {

//              obj.key = key.toString();
//              obj.val = userModel.InteractionLogs[key];
                //logs[count] = obj;
                logs[key] = userModel.InteractionLogs[key];
                count++;
            }
        }
    }
    return logs;
    ;
};
ExportExternProfileFields = function (user) {
    var fields = [];
    if (user == null) {
        user = Meteor.user();
    }
    //"Achiever" "Socializer" "Imersive" "Standard";
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("ExtProfile")) {
            var count = 0;
            for (var key in userModel.ExtProfile) {
                var obj = {};
                fields[key] = userModel.ExtProfile[key];
                //obj.val = userModel.InteractionLogs[key];
                //fields[count] = obj;
                count++;
            }
            return fields;
        }
    }
    return false;
};
GetNumberOfSessions = function (user) {
    if (user == null) {
        user = Meteor.user();
    }

    return UserSessions.find({"userId": user._id}).count();
};
SessionsDataSummary = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var session = {};
    var count = UserSessions.find({"userId": user._id}).count();
    if (count > 0) {
        session.NumberSessions = count;
        session.totalTimeExpent = 0;
        session.meanDuration = 0;

        session.totalQuestions = 0;
        session.totalCorrect = 0;
        session.totalWrong = 0;
        session.meanAnsweredQuestions = 0;
        session.meanCorrectQuestions = 0;
        session.meanWrongQuestions = 0;

        session.totalArousal = 0;
        session.totalControl = 0;
        session.totalHappiness = 0;
        session.meanHappiness = 0;
        session.meanControl = 0;
        session.meanArousal = 0;
        // session.percRight = 0;
        //session.percWrong = 0;
        var Happiness = [];
        var Control = [];
        var Arousal = [];
        var duration = [];
        var Questions = [];
        var CorrectQuestions = [];
        var WrongQuestions = [];
        var n = 0;
        UserSessions.find({"userId": user._id}).forEach(function (ses) {
            if ((ses.hasOwnProperty("totalQuestions")) && (ses.hasOwnProperty("totalCorrect"))) {
                var questions = ses.totalQuestions;
                var corrects = ses.totalCorrect;
                if (((!isNaN(questions)) && (questions)) && ((!isNaN(corrects)) && (corrects))) {
                    session.totalWrong += questions - corrects;
                    WrongQuestions[n] = questions - corrects;
                } else
                    WrongQuestions[n] = 0;
            } else
                WrongQuestions[n] = 0;


            if (ses.hasOwnProperty("totalQuestions")) {
                var val = ses.totalQuestions;
                if ((!isNaN(val)) && (val)) {
                    session.totalQuestions += val;
                    Questions[n] = val;
                } else
                    Questions[n] = 0;
            } else
                Questions[n] = 0;

            if (ses.hasOwnProperty("totalCorrect")) {
                var val = ses.totalCorrect;
                if ((!isNaN(val)) && (val)) {
                    session.totalCorrect += val;
                    CorrectQuestions[n] = val;
                } else
                    CorrectQuestions[n] = 0;
            } else
                CorrectQuestions[n] = 0;

            if (ses.hasOwnProperty("arousal")) {
                var val = ses.arousal;
                if ((!isNaN(val)) && (val)) {
                    session.totalArousal += val;
                    Arousal[n] = val;
                } else {
                    Arousal[n] = 0;
                }
            } else
                Arousal[n] = 0;

            if (ses.hasOwnProperty("control")) {
                var val = ses.control;
                if ((!isNaN(val)) && (val)) {
                    session.totalControl += val;
                    Control[n] = val;
                } else {

                    Control[n] = 0;
                }
            } else
                Control[n] = 0;

            if ((ses.hasOwnProperty("happines"))) {
                var val = ses.happiness;
                if ((!isNaN(val)) && (val)) {
                    session.totalHappiness += val;
                    Happiness[n] = val;
                } else {
                    Happiness[n] = 0;
                }
            } else
                Happiness[n] = 0;

            // console.log(ses);
            var durate = new Date;
            duration[n] = 0;
            if (ses.hasOwnProperty('finished')) {

                durate = ses.finished.getTime() - ses.start.getTime();
                //session.totalDuration += math.abs(session.finished.getTime() - session.start.getTime())/60;
//           session.duration = Math.floor(durate / 60000);       
                //    console.log(ses.finished.getTime() + "  " + ses.start.getTime() + "  " + durate);
                var mins = math.ceil(((durate / 1000) / 60) % 60);
                session.totalTimeExpent += mins;
                duration[n] = ((durate / 1000) / 60) % 60;
            } else if (ses.hasOwnProperty('lastseen')) {
                durate = ses.lastseen.getTime() - ses.start.getTime();
                //session.totalDuration += math.abs(session.lastseen.getTime() - session.start.getTime());

                //      console.log(ses.lastseen.getTime() + "  " + ses.start.getTime() + "  " + durate);
                var mins = math.ceil(((durate / 1000) / 60) % 60);
                session.totalTimeExpent += mins;
                duration[n] = ((durate / 1000) / 60) % 60;
            }


            n++;
        });

        session.meanCorrectQuestions = math.mean(CorrectQuestions);
        session.meanWrongQuestions = math.mean(WrongQuestions);
        session.meanAnsweredQuestions = math.mean(Questions);
        session.meanDuration = math.mean(duration);
        session.meanHappiness = math.mean(Happiness);
        session.meanControl = math.mean(Control);
        session.meanArousal = math.mean(Arousal);
        //    session.medianQuestions = math.median(Questions);

        //    session.medianDuration = math.median(duration);

        return session;
    } else {
        return false;
    }
};
ExportRoutes = function () {

    var session = {};
    var count = UserSessions.find({}).count();
    var array = [];

    if (count > 0) {

        var n = 0;
        var query = {};
        query.query = {};
        query.sort = {"userId": -1};

        UserSessions.find(query.query, query.sort).forEach(function (ses) {
            var path = {};

            var NumDefaultVal = 0;
            var StrDefaultVal = "none";
            var UniqueDefaultVal = null;


            //prepareat least 10 routes

            path["_id"] = UniqueDefaultVal;
//            path["username"] = userGetUsername({_id: ses.userId});            
            path["username"] = UniqueDefaultVal;
            path["ExperimentalGroup"] = 0;
            path["ExperimentalGroup-formated"] = 0;
            path["idade"] = UniqueDefaultVal;
            path["genero"] = -1;
            path["genero-formated"] = -1;
            //path["escolaridade"] = -1;
            path["GrupoOrigem"] = UniqueDefaultVal;
            path["escolaridade"] = -1;
            path["escolaridade-formated"] = -1;
            path["start"] = UniqueDefaultVal;
            path["end"] = UniqueDefaultVal;
            path["duration"] = NumDefaultVal;
            path["arousal"] = UniqueDefaultVal;
            path["control"] = UniqueDefaultVal;
            path["happiness"] = UniqueDefaultVal;
            path["WrongQuestions"] = NumDefaultVal;
            path["CorrectQuestions"] = NumDefaultVal;
            path["Questions"] = NumDefaultVal;
            path["comment"] = UniqueDefaultVal;


            if (ses.hasOwnProperty("userId")) {

//                }
                var val =  {_id: ses.userId};
                path["_id"] = ses.userId;
                path["username"] = userGetUsername(val);

                var grp = GetExperimentalGroup(val);
//                if(grp) {
                path["ExperimentalGroup"] = (grp);
                path["ExperimentalGroup-formated"] = formatExpGroup(grp);

                var profile = formatProfiles(GetUserProfileName(val));
                //if (profile)
                path["profileName"] = GetUserProfileName(val);
                path["profileName-formated"] = profile;
//        else
                //          user.profileName = "none";

                var extProfile = formatProfiles(GetUserExternProfileName(val));
                //if (extProfile)
                path["ExternProfile"] = GetUserExternProfileName(val);
                path["ExternProfile-formated"] = extProfile;

                //if((path.username.toString().toLowerCase()=="fernando.heb@gmail.com") || (path.username.toString().toLowerCase()=="teste@gmail.com"))                {                    return;                }

                var idade = GetUserExternProfileIdade(val);
                if (idade)
                    path["idade"] = idade;
                // else
                //   path["idade"] = null;
                var genero = GetUserExternProfileGenero(val);
//                if (genero){
                path["genero"] = (genero);
                path["genero-formated"] = formatGender(genero);
//            }
                //else
                //  path["genero"] = "none";
                var GrupoOrigem = GetUserExternProfileGrupoOrigem(val);
//                if (GrupoOrigem){
                path["GrupoOrigem"] = (GrupoOrigem);
                path["GrupoOrigem-formated"] = formatProfiles(GrupoOrigem);
//                }
                //else
                //  path["GrupoOrigem"] = "none";
                var escolaridade = GetUserExternProfileEscolaridade(val);
//                if (escolaridade) {
                path["escolaridade"] = (escolaridade);
                path["escolaridade-formated"] = formatScholarship(escolaridade);
//                }
                //else
                //  path["escolaridade"] = "none";

            }
//             var group = formatExpGroup(path["ExperimentalGroup"]) + "_";
//             var UnFormatedGroup = path["ExperimentalGroup"];
            // if(!group)
            //     group = "none_";

            path["start"] = ses.start;
            var durate = new Date;
            if (ses.hasOwnProperty('finished')) {
                path["end"] = ses.finished;
                durate = ses.finished.getTime() - ses.start.getTime();
                var mins = math.ceil(((durate / 1000) / 60) % 60);
                path["duration"] = mins;
            } else if (ses.hasOwnProperty('lastseen')) {
                durate = ses.lastseen.getTime() - ses.start.getTime();
                path["end"] = ses.lastseen;
                var mins = math.ceil(((durate / 1000) / 60) % 60);
                path["duration"] = mins;
            }

            if (ses.hasOwnProperty("comment")) {
                path["comment"] = ses.comment.toString();
            } //else {
            //path["comment"] = "";
            //}
            if (ses.hasOwnProperty("arousal")) {
                var val = ses.arousal;
                if ((!isNaN(val)) && (val)) {
                    //   console.log(val);
                    path["arousal"] = val;
                }// else {
                // path["arousal"] = 0;
                //}
            }
            if (ses.hasOwnProperty("control")) {
                var val = ses.control;
                if ((!isNaN(val)) && (val)) {
                    path["control"] = val;
                }
//                 else {
//                    path["control"] = null;
//                }
            }
            if (ses.hasOwnProperty("happines")) {
                var val = ses.happiness;
                if ((!isNaN(val)) && (val)) {
                    path["happiness"] = val;
                }
//                //else {
//                    path["happiness"] = null;
//                }
            }

            if ((ses.hasOwnProperty("totalQuestions")) && (ses.hasOwnProperty("totalCorrect"))) {
                var questions = ses.totalQuestions;
                var corrects = ses.totalCorrect;
                if (((!isNaN(questions)) && (questions)) && ((!isNaN(corrects)) && (corrects))) {
                    path["WrongQuestions"] = questions - corrects;
                }
//                else                   
//                    path["WrongQuestions"] = defaultval;
            }

            if (ses.hasOwnProperty("totalQuestions")) {
                var val = ses.totalQuestions;
                if ((!isNaN(val)) && (val)) {
                    path["Questions"] = val;
                }
//                else
//                    path["Questions"] = 0;
            }


            if (ses.hasOwnProperty("totalCorrect")) {
                var val = ses.totalCorrect;
                if ((!isNaN(val)) && (val)) {

                    path["CorrectQuestions"] = val;
                }
//                else
//                    path["CorrectQuestions"] = 0;
            }
////            for (var i = 0; i <= 12; i++) {
//                path["origem-" + i] = "";
//                path["destino-" + i] = "";
//            }


            /* path["leaderboard"] = NumDefaultVal;
             path["personalStatus"] = NumDefaultVal;
             path["achievementBoard"] = NumDefaultVal;
             path["Achievements"] = NumDefaultVal;
             path["AchievementsCreation"] = NumDefaultVal;
             path["ExpendedPoints"] = NumDefaultVal;
             path["gameOn"] = NumDefaultVal;
             path["GameOn"] = NumDefaultVal;
             path["hasOption"] = NumDefaultVal;
             path["levelTotal"] = NumDefaultVal;
             path["pointsTotal"] = NumDefaultVal;
             path["showCallChalange"] = NumDefaultVal;
             path["showHealth"] = NumDefaultVal;
             path["QuestionText"] = NumDefaultVal;
             path["AskedHint"] = NumDefaultVal;
             path["Study"] = NumDefaultVal;
             path["custo 0"] = NumDefaultVal;
             path["custo 50"] = NumDefaultVal;
             path["custo 100"] = NumDefaultVal;
             path["custo 150"] = NumDefaultVal;
             path["custo 200"] = NumDefaultVal;
             path["custo 250"] = NumDefaultVal;
             path["custo 300"] = NumDefaultVal;
             path["custo 350"] = NumDefaultVal;
             path["custo 400"] = NumDefaultVal;
             path["custo 450"] = NumDefaultVal;
             path["custo 500"] = NumDefaultVal;
             path["custo 550"] = NumDefaultVal;
             path["custo 600"] = NumDefaultVal;
             path["custo 650"] = NumDefaultVal;
             path["custo 700"] = NumDefaultVal;
             path["custo 750"] = NumDefaultVal;
             path["custo 800"] = NumDefaultVal;
             path["custo 3000"] = NumDefaultVal;*/

            //console.log(userModel);
            if (ses.hasOwnProperty("InteractionLogs")) {
                var count = 0;
                for (var key in ses.InteractionLogs) {

//              obj.key = key.toString();
//              obj.val = userModel.InteractionLogs[key];
                    //logs[count] = obj;

                    path[key] = ses.InteractionLogs[key];

                    count++;
                }
            }

            if (ses.hasOwnProperty("routes"))
                for (var i in ses.routes) {
                    if ((i == 0) && (ses.routes[i].origem == null)) {
                        path["origem-" + i] = "start";
                    } else
                        path["origem-" + i] = ses.routes[i].origem;
                    path["destino-" + i] = ses.routes[i].destino;
                }

            /***/


//          createNullFields("idade",null,function(obj){ for(var i in obj) path[i] = obj[i]; });
//          createNullFields("duration",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("arousal",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("control",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("happiness",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("wrongQuestions",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("CorrectQuestions",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("Questions",null,function(obj){for(var i in obj) path[i] = obj[i];});
//            if(path["idade"])
//            path[group + "idade"] = path["idade"];
//            if(path["duration"])
//            path[group + "duration"] = path["duration"];
//           if(path["arousal"])
//            path[group + "arousal"] = path["arousal"];
//            if(path["control"])
//            path[group + "control"] = path["control"];
//             if(path["happiness"])
//            path[group + "happiness"] = path["happiness"];
//            if(path["wrongQuestions"])
//            path[group + "WrongQuestions"] = path["WrongQuestions"];
//            if(path["CorrectQuestions"])
//                path[group + "CorrectQuestions"] = path["CorrectQuestions"];
//           if(path["Questions"])
//            path[group + "Questions"] = path["Questions"];
//            if (ses.hasOwnProperty("InteractionLogs")) {
//                var count = 0;
//                for (var key in ses.InteractionLogs) {
//
////              obj.key = key.toString();
////              obj.val = userModel.InteractionLogs[key];
//                    //logs[count] = obj;
//                    createNullFields(key,0,function(obj){for(var i in obj) path[i] = obj[i];});
//                    path[group + key] = ses.InteractionLogs[key];
//                    count++;
//                }
//            }
            /***/
            array[n] = path;
            n++;
        });
    }

    return array;
};

ExportSessions = function (user) {

    var session = {};
    var count = UserSessions.find({}).count();
    var array = [];

    if (count > 0) {

        var n = 0;
        var query = {};
        query.query = {};
        query.sort = {"userId": -1};

        UserSessions.find(query.query, query.sort).forEach(function (ses) {
            var path = {};

            var NumDefaultVal = 0;
            var StrDefaultVal = "none";
            var UniqueDefaultVal = null;
           var val ={_id:ses.userId};     

            //prepareat least 10 routes
            if (ses.hasOwnProperty("userId")) {
                var grp = GetExperimentalGroup(val);


                // else
                //   path["ExperimentalGroup"] = "none";

//                var group = path["ExperimentalGroup"] + "_";
                //   if(!group)
                //    group = "none";
                path["_id"] = ses.userId;
//                path[group+"username"] = userGetUsername(val);
                path["username"] = userGetUsername(val);
                var GrupoOrigem = GetUserExternProfileGrupoOrigem(val);
                if (GrupoOrigem) {
                    path["GrupoOrigem"] = (GrupoOrigem);
//                    path["GrupoOrigem-formated"] = formatProfiles(GrupoOrigem);
//                    path[group+"GrupoOrigem"] = formatProfiles(GrupoOrigem);
                }
                //else
                //  path["GrupoOrigem"] = "none";
                if (grp)
                    path["ExperimentalGroup"] = (grp);
                path["ExperimentalGroup-"] = formatExpGroup(grp);

                var profile = formatProfiles(GetUserProfileName(val));
                //if (profile)
                path["profileName"] = GetUserProfileName(val);
                path["profileName-formated"] = profile;
//        else
                //          user.profileName = "none";

                var extProfile = formatProfiles(GetUserExternProfileName(val));
                //if (extProfile)
                path["ExternProfile"] = GetUserExternProfileName(val);
                path["ExternProfile-formated"] = extProfile;
                //else        //  user.ExternProfile = "none";

//          createNullFields("idade",null,function(obj){ for(var i in obj) path[i] = obj[i]; });
//          createNullFields("duration",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("arousal",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("control",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("happiness",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("wrongQuestions",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("CorrectQuestions",null,function(obj){for(var i in obj) path[i] = obj[i];});
//          createNullFields("Questions",null,function(obj){for(var i in obj) path[i] = obj[i];});
                var idade = GetUserExternProfileIdade(val);
                if (idade)
                    path["idade"] = idade;
//                    path[group+"idade"] = idade;
                // else
                //   path["idade"] = null;
                var genero = GetUserExternProfileGenero(val);
                if (genero) {
                    path["genero"] = (genero);
                    path["genero-formated"] = formatGender(genero);
//                    path[group+"genero"] = formatGender(genero);
                }
                //else
                //  path["genero"] = "none";

                var escolaridade = GetUserExternProfileEscolaridade(val);
                if (escolaridade) {
                    path["escolaridade"] = (escolaridade);
                    path["escolaridade-formated"] = formatScholarship(escolaridade);
//                    path[group+"escolaridade"] = formatScholarship(escolaridade);
                }
                //else
                //  path["escolaridade"] = "none";

            }

            path["start"] = ses.start;
            var durate = new Date;
            if (ses.hasOwnProperty('finished')) {
                path["end"] = ses.finished;
                durate = ses.finished.getTime() - ses.start.getTime();
                var mins = math.ceil(((durate / 1000) / 60) % 60);
                path["duration"] = mins;
//                path[group+"duration"] = mins;
            } else if (ses.hasOwnProperty('lastseen')) {
                durate = ses.lastseen.getTime() - ses.start.getTime();
                path["end"] = ses.lastseen;
                var mins = math.ceil(((durate / 1000) / 60) % 60);
                path["duration"] = mins;
//                path[group+"duration"] = mins;
            }

            if (ses.hasOwnProperty("comment")) {
                path["comment"] = ses.comment.toString();
            } //else {
            //path["comment"] = "";
            //}
            if (ses.hasOwnProperty("arousal")) {
                var val = ses.arousal;
                if ((!isNaN(val)) && (val)) {
                    //   console.log(val);
                    path["arousal"] = val;
//                    path[group+"arousal"] = val;
                }// else {
                // path["arousal"] = 0;
                //}
            }
            if (ses.hasOwnProperty("control")) {
                var val = ses.control;
                if ((!isNaN(val)) && (val)) {
                    path["control"] = val;
//                    path[group+"control"] = val;
                }
//                 else {
//                    path["control"] = null;
//                }
            }
            if (ses.hasOwnProperty("happines")) {
                var val = ses.happiness;
                if ((!isNaN(val)) && (val)) {
                    path["happiness"] = val;
//                    path[group+"happiness"] = val;
                }
//                //else {
//                    path["happiness"] = null;
//                }
            }

            if ((ses.hasOwnProperty("totalQuestions")) && (ses.hasOwnProperty("totalCorrect"))) {
                var questions = ses.totalQuestions;
                var corrects = ses.totalCorrect;
                if (((!isNaN(questions)) && (questions)) && ((!isNaN(corrects)) && (corrects))) {
                    path["WrongQuestions"] = questions - corrects;
                }
//                else                   
//                    path["WrongQuestions"] = defaultval;
            }

            if (ses.hasOwnProperty("totalQuestions")) {
                var val = ses.totalQuestions;
                if ((!isNaN(val)) && (val)) {
                    path["Questions"] = val;
                }
//                else
//                    path["Questions"] = 0;
            }


            if (ses.hasOwnProperty("totalCorrect")) {
                var val = ses.totalCorrect;
                if ((!isNaN(val)) && (val)) {

                    path["CorrectQuestions"] = val;
                }
            }
//            /***/
//            if(path["idade"])
//            path[group + "idade"] = path["idade"];
//        if(path["duration"])
//            path[group + "duration"] = path["duration"];
//        if(path["arousal"])
//            path[group + "arousal"] = path["arousal"];
//        if(path["control"])
//            path[group + "control"] = path["control"];
//        if(path["happiness"])
//            path[group + "happiness"] = path["happiness"];
//        if(path["wrongQuestions"])
//            path[group + "WrongQuestions"] = path["WrongQuestions"];
//        if(path["CorrectQuestions"])
//            path[group + "CorrectQuestions"] = path["CorrectQuestions"];
//        if(path["Questions"])
//            path[group + "Questions"] = path["Questions"];
            if (ses.hasOwnProperty("InteractionLogs")) {
                var count = 0;
                for (var key in ses.InteractionLogs) {

//              obj.key = key.toString();
//              obj.val = userModel.InteractionLogs[key];
                    //logs[count] = obj;

                    path[key] = ses.InteractionLogs[key];
//                    path[group + key] = ses.InteractionLogs[key];

                    count++;
                }
            }
            /***/
            array[n] = path;
            n++;
        });
    }

    return array;
};
GetUserShowLeaderBoard = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("leaderboard")) {
            return userModel.leaderboard;
        }
    }
    return false;
};
GetUserShowPersonalStatus = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("personalStatus")) {
            return userModel.personalStatus;
        }
    }
    return false;
};
GetUserShowAchievementBoard = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("achievementBoard")) {
            return userModel.achievementBoard;
        }
    }
    return false;
};
GetUserShowCreateAchievement = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("ShowCreateAchievement")) {
            return userModel.ShowCreateAchievement;
        }
    }
    return false;
};
GetUserShowLevel = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("level")) {
                return  userModel.scoreRules.level;
            }
        }
    }
    return false;
};
GetUserShowProfile = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("profileOn")) {
            return userModel.profileOn;
        }
    }
    return false;
};
GetUserShowAvatar = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("avatar")) {
            return userModel.avatar;
        }
    }
    return false;
};
GetUserShowHealth = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    if (userModel != null) {
        if (userModel.hasOwnProperty("showHealth")) {
            return userModel.showHealth;
        }
    }
    return false;
};
GetUserShowBackground = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("background")) {
            return userModel.background;
        }
    }
    return false;
};
GetUserLevelDificulty = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("levelDificulty")) {
                return  userModel.scoreRules.levelDificulty;
            }
        }
    }
    return 0.3;
};
GetUserRatio = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("ratio")) {
                return  userModel.scoreRules.ratio;
            }
        }
    }
    return 0.2;
};
GetUserGameOn = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("gameOn")) {
            return userModel.gameOn;
        }
    }
    return false;
};
GetUserGameChance = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("gameChance")) {
                return  userModel.scoreRules.gameChance;
            }
        }
    }
    return 0.5;
};
GetUserShowCallChalange = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    //console.log(userModel);
    if (userModel != null) {
        if (userModel.hasOwnProperty("showCallChalange")) {
            return userModel.showCallChalange;
        }
    }
    return false;
};
GetUserHealth = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    if (userModel != null) {
        if (userModel.hasOwnProperty("Health")) {
            return userModel.Health;
        }
    }
    return null;
};
GetUserMaxHealth = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    if (userModel != null) {
        if (userModel.hasOwnProperty("MaxHealth")) {
            return userModel.MaxHealth;
        } else {
            return 100;
        }
    }
    return null;
};
GetUserDamageType = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("damageType")) {
                return  userModel.scoreRules.damageType;
            }
        }
    }
    return null;
};
GetUserDamageOnTimeOut = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("damageOnTimeOut")) {
                return  userModel.scoreRules.damageOnTimeOut;
            }
        }
    }
    return null;
};
GetUserDamageValue = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("damageValue")) {
                return  userModel.scoreRules.damageValue;
            }
        }
    }
    return 0;
};
GetUserRecoverHealthValue = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("recoverHealthValue")) {
                return  userModel.scoreRules.recoverHealthValue;
            }
        }
    }
    return 0;
};
GetUserRecoverHealthType = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(Meteor.user());
    if (userModel != null) {
        if (userModel.hasOwnProperty("scoreRules")) {
            if (userModel.scoreRules.hasOwnProperty("recoverHealthType")) {
                return  userModel.scoreRules.recoverHealthType;
            }
        }
    }
    return null;
};
//Easy Switch one Element behavior
SetGamificationProp = function (user, prop, value) {
    if (user == null) {
        user = Meteor.user();
    }
    if ((!prop) || (typeof (prop) !== "string"))
        return false;
    RegisterInteraction(Meteor.user(), prop);
    var obj = {'$set': {prop: value}};
    Meteor.call("connGamification", user._id, obj, "update");
};
//Easy Switch one Element behavior
SwitchUserGameOn = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "gameOn");
    var obj = {'$set': {'gameOn': !GetUserGameOn(user)}};
    Meteor.call("connGamification", user._id, obj, "update");
};
SwitchUserShowCallChalange = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "showCallChalange");
    var obj = {'$set': {'showCallChalange': !GetUserShowCallChalange(user)}};
    Meteor.call("connGamification", user._id, obj, "update");
};
SwitchUserScore = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "score");
    var obj = {'$set': {'score': !GetUserShowScore(user)}};
    Meteor.call("connGamification", user._id, obj, "update");
};
SwitchUserLeaderboard = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "leaderboard");
    var obj = {'$set': {'leaderboard': !GetUserShowLeaderBoard(user)}};
    Meteor.call("connGamification", user._id, obj, "update");
};
SwitchUserPersonalStatus = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "personalStatus");
    var obj = {'$set': {'personalStatus': !GetUserShowPersonalStatus(user)}};
    Meteor.call("connGamification", user._id, obj, "update");
};
SwitchUserHasOption = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "hasOption");
    var obj = {'$set': {'hasOption': !GetUserHasOption(user)}};
    Meteor.call("connGamification", user._id, obj, "update");
};
SwitchUserAchievementBoard = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "achievementBoard");
    var obj = {'$set': {'achievementBoard': !GetUserShowAchievementBoard(user)}};
    Meteor.call("connGamification", user._id, obj, "update");
};
SwitchUserLevel = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "level");
    var obj = {'$set': {'scoreRules': {'level': !GetUserShowLevel(user)}}};
    Meteor.call("connGamification", user._id, obj, "update");
};
SwitchShowHealth = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    RegisterInteraction(Meteor.user(), "showHealth");
    var obj = {'$set': {'showHealth': !GetUserShowHealth(user)}};
    Meteor.call("connGamification", user._id, obj, "update");
};
//ldk:bootstrap-progressbar

/*
 UserScore = function (user, tdfname) {
 var indivUserQuery = {'_id': user._id};
 //console.log("compute score user ");
 //console.log(indivUserQuery);
 // We use findOne because there should only ever be one user with any given id.
 var indivUser = UserMetrics.findOne(indivUserQuery);
 //console.log(indivUser);
 
 var score = [];
 if (tdfname != null) {
 var tdf = buildTdfDBName(tdfname);
 var tdfData = {};
 tdfData.askCount = 0;
 tdfData.correctCount = 0;
 tdfData.tdfName = tdf;
 _.chain(indivUser).prop(tdf).each(function (item) {
 tdfData.askCount += _.chain(item).prop('questionCount').intval().value();
 tdfData.correctCount += _.chain(item).prop('correctAnswerCount').intval().value();
 });
 score.push(tdfData);
 } else {
 var tdfs = userAnsweredTDFS(user);
 //var count = 0;
 tdfs.forEach(function (tdf) {
 var tdfData = {};
 tdfData.askCount = 0;
 tdfData.correctCount = 0;
 _.chain(indivUser).prop(tdf).each(function (item) {
 tdfData.tdfName = tdf;
 tdfData.askCount += _.chain(item).prop('questionCount').intval().value();
 tdfData.correctCount += _.chain(item).prop('correctAnswerCount').intval().value();
 });
 score.push(tdfData);
 //count++;
 });
 }
 
 //var tryValues = getCurrentScoreValues();
 //console.log( "try calues "+Values[0]);
 return score; // Note the possible of DBZ which would return NaN
 };
 UserGetScore = function (user, tdfname) {
 var userModel = GetUserModel(user);
 if (userModel == null)
 return 0;
 var ratio = 0.8; //defaultCase 1-0.2
 if (userModel.hasOwnProperty("scoreRules")) {
 if (userModel.scoreRules.hasOwnProperty("ratio")) {
 ratio = 1 - userModel.scoreRules.ratio;
 }
 }
 
 var score = UserScore(user, tdfname);
 //var point = score[0].correctCount * ratio; //Math.round(Math.pow(score[0].correctCount,2)/score.askCount);
 //return point;           
 var totalAsked = 0;
 var totalCorrect = 0;
 score.forEach(function (res) {
 totalAsked += res.askCount;
 totalCorrect += res.correctCount;
 });
 var point = totalCorrect * ratio; //Math.round(Math.pow(totalCorrect,2)/totalAsked);
 return Math.round(point);
 };
 UserGetTotalScore = function (user) {
 var userModel = GetUserModel(user);
 if (userModel == null)
 return 0;
 
 var ratio = 0.8; //defaultCase 1-0.2
 if (userModel.hasOwnProperty("scoreRules")) {
 if (userModel.scoreRules.hasOwnProperty("ratio")) {
 ratio = 1 - userModel.scoreRules.ratio;
 }
 }
 var score = UserScore(user);
 var totalAsked = 0;
 var totalCorrect = 0;
 score.forEach(function (res) {
 totalAsked += res.askCount;
 totalCorrect += res.correctCount;
 });
 var point = totalCorrect * ratio; //Math.round(Math.pow(totalCorrect,2)/totalAsked);
 return Math.round(point);
 };
 UserGettGamifictionAllStatus = function (user) {
 var userProfile = {};
 userProfile = Gamification.findOne({_id: user._id});
 };
 */
/*composeLeaderboard = function (tdfName) { // old 
 var students = [];
 var BuildedTdfName = buildTdfDBName(tdfName);
 var studentsIn = StudentsInTDF(BuildedTdfName);
 studentsIn.forEach(function (student) {
 var userModel = GetUserModel(student);
 if (userModel == null)
 return;
 student.score = UserGetScore(student, BuildedTdfName);
 student.username = userGetUsername(student);
 students.push(student);
 });
 
 //students.buttonColor = determineButtonColor(student.score);
 //console.log(student);
 
 //console.log("buttons");
 //console.log(buttons);
 return students;
 };*/
//UserGetLevel = function (user, tdfname) {
//    if (user == null) {
//        user = Meteor.user();
//    }
//    var userModel = GetUserModel(user);
//    if (userModel == null)
//        return {"xp": 0, "level": 0};
//    //var level = UserGetScore(user);
//    var ratio = 0.8; //defaultCase 1-0.2
//    if (userModel.hasOwnProperty("scoreRules")) {
//        if (userModel.scoreRules.hasOwnProperty("levelDificulty")) {
//            ratio = 1 - userModel.scoreRules.levelDificulty;
//        }
//    }
//
//    var score = UserScore(user, tdfname);
//    var totalAsked = 0;
//    var totalCorrect = 0;
//    score.forEach(function (res) {
//        totalAsked += res.askCount;
//        totalCorrect += res.correctCount;
//    });
//    var xp = (totalCorrect);
//    var level;
//    if (xp > 0) {
//        level = Math.round(Math.log2(xp * ratio));
//    } else {
//        level = 1;
//    }
//    /* if (level < 0) {
//     level = 0;   
//     }*/
//    return {"xp": xp, "level": level};
//    /*         {
//     "_id": "aHwKJdoRuopJrWRXf",
//     "leaderboard": true,
//     "personalStatus": false,
//     "score": true,
//     "achievementBoard": true,
//     "scoreRules": {
//     "level": true,
//     "ratio": 0.2,
//     "levelDificulty": 0.3
//     }
//     */
//};