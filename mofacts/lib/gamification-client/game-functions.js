//////////////////////////////////////////////////////////////////////////
// Helpful utility functions for Meteor users
//Verify if the Gamification Profile Exists and try to Locate the answers and create if its exists in the questionnaire database.
var srcAchievementUnlocked = '/sounds/effects/sonic_the_hedgehog';
var srcNewAchievement = '/sounds/effects/moogle_magnet_tone.mp3';
//var srcSoundPoints = '/sounds/effects/mario_coin_sound.mp3';
var srcSoundPoints = '/sounds/effects/sonic___rings.mp3';
var srcSoundDamage = '/sounds/effects/Cartoon_Boing.mp3';

verificaGamificationProfile = function (isTest, user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    if (userModel == null) {
        Meteor.call('verifyUserModel', isTest, user, function (Error, result) {
            if (Error) {
                console.log('Erro na chamada de verifyUserModel');
                console.log(Error);
                startSession(user);
            } else {
                console.log("verifica Modelo Usuario (y)");
                startSession(user);

            }
        });
        //startSession(user);
    } else {
        console.log("modelo do usuario encontrado pelo Gamification.find()");
        startSession(user);
    }

}

userAllStatusAVG = function (user) {
    if (!user)
        return null;
    var indivUser = UserMetrics.findOne({_id: user._id});
    var tdfs = userAnsweredTDFS(user);
    var count = 0;
    var status = [];
    tdfs.forEach(function (tdf) {
        status[count].askCount = 0;
        status[count].correctCount = 0;
        _.chain(indivUser).prop(tdf).each(function (item) {
            status[count].tdf = tdf;
            status[count].askCount += _.chain(item).prop('questionCount').intval().value();
            status[count].correctCount += _.chain(item).prop('correctAnswerCount').intval().value();
        });
    });
    return status;
};
usersAvailableTDFS = function () {
    var userAvailableTdfs = [];
    var tdf = Tdfs.find({});
    var count = 0;
    tdf.forEach(function (tdfObject) {
        count++;
        var setspec = _.chain(tdfObject)
                .prop("tdfs")
                .prop("tutor")
                .prop("setspec").first()
                .value();
        if (setspec.hasOwnProperty("userselect")) {
            if (setspec.userselect[0] = "true") {
                userAvailableTdfs.push(buildTdfDBName(tdfObject.fileName));
            }
        }
    });
    return userAvailableTdfs;
};
userAnsweredTDFS = function (user) {
    if (!user)
        return null;
    var tdfs = usersAvailableTDFS();
    var answered = [];
    tdfs.forEach(function (tdf) {
        var userMetric = UserMetrics.findOne(user._id);
        if (userMetric.hasOwnProperty(tdf))
            answered.push(tdf);
    }
    );
    return answered;
};
StudentsInTDF = function (tdfname) {
    var userQuery = {};
    var tdfDBName = buildTdfDBName(tdfname);
    userQuery[tdfDBName] = {'$exists': true};
    //{scores.HiraganaOptim_xml : {$exists:true} }
    var Students = UserMetrics.find(userQuery);
    return Students;
};
StudentsInTDF_withScores = function (tdfname) {
    var userQuery = {};
    var tdfDBName = buildTdfDBName(tdfname);
    userQuery[tdfDBName] = {'$exists': true};
    //console.log(userQuery);
    //{scores.HiraganaOptim_xml : {$exists:true} }
    var Students = Scores.find(userQuery);
    return Students;
};
IncreaseUserScore = function (user, tdfName) {
    var soundPoints = new buzz.sound(srcSoundPoints);
    if (user == null) {
        user = Meteor.user();
    }
    var tdfname = buildTdfDBName(tdfName);
    var userModel = GetUserModel(user);
    if (userModel != null) {


        var userScores = GetUserScores(user);
        if (!userScores) {
            userScores = {_id: user._id};
            userScores.totalPoints = 0;
            userScores.totalLevel = 0;
        }
        if (!userScores.hasOwnProperty(tdfname)) {
            userScores[tdfname] = {};
            userScores[tdfname].points = 0;
            userScores[tdfname].level = 0;
        }
        var oldPoints = userScores[tdfname].points;
        var oldLevel = userScores[tdfname].level;
        //calcula domain (tdfname) points
        userScores[tdfname].points = increasePoints(userModel, oldPoints, oldLevel);
        //calcula level
        userScores[tdfname].level =
                increaseLevel(userModel,
                        userScores[tdfname].level,
                        userScores[tdfname].points);
        //calculate TotalPoints
        userScores.totalPoints += userScores[tdfname].points - oldPoints;
        //calculate totallevel
        userScores.totalLevel = increaseTotalLevel(userModel, userScores.totalLevel, userScores.totalPoints);
        Meteor.call('connScores', userScores._id, userScores, "upsert");
        userScores.oldPoints = oldPoints;
        userScores.lastPointsGot = userScores[tdfname].points - oldPoints; //pass field with the last points got 
//        toastrSuccessConfig();
//        toastr.success("+" + userScores.lastPointsGot + " pontos", "Você ganhou");
        checkAchievements(user, tdfname);
        soundPoints.play();
        return userScores;
    }
};

ExpendPoints = function (user, custo, tdf, lowPointsMsg) {
    if (user == null) {
        user = Meteor.user();
    }
    if ((tdf == null) || (tdf === "null")) {
        tdf = null;
    }
    var lvl = 0;
    if (tdf == null)
        lvl = GetUserTotalLevel(null);
    else
        lvl = GetUserLevel(null, tdf);


    if ((GetUserTotalPoints(null) < custo) && (lvl > 5)) {

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "500",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": 'slideDown',
            "hideMethod": 'slideUp',
            "closeMethod": 'slideUp',
            escapeHtml: false
        };
        var msgStyled = "<h1><center>" + lowPointsMsg + "</center></h1>";
        toastr.error(msgStyled, "$$ insuficiente");
        var SoundDamage = new buzz.sound(srcSoundDamage);
        SoundDamage.play();
        return false;
    }
    ReduceUserScore(null, tdf, custo); //colocar informação do custo na tela 
    RegisterInteraction(Meteor.user(), "ExpendedPoints");
    RegisterInteraction(Meteor.user(), "custo " + custo);

};
ReduceUserScore = function (user, tdfname, value) {

    if (user == null) {
        user = Meteor.user();
    }

    if ((tdfname == null) || (tdfname === "null"))
        tdfname = null;
    else
        tdfname = buildTdfDBName(tdfname);

    var userModel = GetUserModel(user);
    if (userModel == null)
        return false;

    var userScores = GetUserScores(user);
    if (!userScores)
        return false;

    var obj = {};
    if (tdfname !== null) {
        if (!userScores.hasOwnProperty(tdfname)) {
            userScores[tdfname] = {};
            userScores[tdfname].points = 0;
        } else {
            userScores[tdfname].points -= value;
        }
    }
    userScores.totalPoints -= value;
    Meteor.call('connScores', userScores._id, userScores, "upsert");


    checkAchievements(user, tdfname);
    return userScores;
};
toastrInfoConfig = function () {
    toastr.clear();
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "200",
        "hideDuration": "200",
        "timeOut": "5000",
        "extendedTimeOut": "3000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": 'slideDown',
        "hideMethod": 'slideUp',
        "closeMethod": 'slideUp'
//        "showMethod": "fadeIn",
//        "hideMethod": "fadeOut"
//  https://atmospherejs.com/chrismbeckett/toastr
    };
};
toastrWarningConfig = function () {
    toastr.clear();
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "200",
        "hideDuration": "200",
        "timeOut": "5000",
        "extendedTimeOut": "3000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": 'slideDown',
        "hideMethod": 'slideUp',
        "closeMethod": 'slideUp'
//        "showMethod": "fadeIn",
//        "hideMethod": "fadeOut"
//  https://atmospherejs.com/chrismbeckett/toastr
    };
};
toastrErrorConfig = function () {
    toastr.clear();
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-Center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "200",
        "timeOut": "5000",
        "extendedTimeOut": "3000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": 'slideDown',
        "hideMethod": 'slideUp',
        "closeMethod": 'slideUp'
//        "showMethod": "fadeIn",
//        "hideMethod": "fadeOut"
//  https://atmospherejs.com/chrismbeckett/toastr
    };
};
toastrSuccessConfig = function () {
    toastr.clear();
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-bottom-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "500",
        "timeOut": "5000",
        "extendedTimeOut": "2000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
//  https://atmospherejs.com/chrismbeckett/toastr
    };
};
toastrconfigDefault = function () {
    toastr.clear();
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "500",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": 'slideDown',
        "hideMethod": 'slideUp',
        "closeMethod": 'slideUp'
//  https://atmospherejs.com/chrismbeckett/toastr
    };
};
increasePoints = function (userModel, oldPoints, userLevel) {
    var newPoints = oldPoints;
    var ratio = 0.8; //defaultCase 1-0.2
    if (userModel.hasOwnProperty("scoreRules")) {
        if (userModel.scoreRules.hasOwnProperty("ratio")) {
            ratio = 1 - userModel.scoreRules.ratio;
        }
    }
    if (userLevel > 10) {
        ratio = (ratio + (userLevel / 10));
    }
    var MaxPoint = 100;
    var point = Math.round(MaxPoint / ratio); //Math.round(Math.pow(totalCorrect,2)/totalAsked);

    return newPoints + point;
};
increaseLevel = function (userModel, oldLevel, newPoints) {
    var newLevel = oldLevel;
    var ratio = 0.8; //defaultCase 1-0.2
    if (userModel.hasOwnProperty("scoreRules")) {
        if (userModel.scoreRules.hasOwnProperty("levelDificulty")) {
            ratio = 1 - userModel.scoreRules.levelDificulty;
        }
    }
    newLevel = Math.round(Math.log2(newPoints * ratio));
    //  if (newLevel > oldLevel )  increaseRatio(domain?)
    if (newLevel > oldLevel) {
        /* 
         var oldPointsRatio = userModel.scoreRules.ratio;
         var newPointsRatio = oldPointsRatio + newLevel/100;
         */
    }
    return newLevel;
};
increaseTotalLevel = function (userModel, oldLevel, newPoints) {
    var newLevel = oldLevel;
    var ratio = 0.8; //defaultCase 1-0.2
    if (userModel.hasOwnProperty("scoreRules")) {
        if (userModel.scoreRules.hasOwnProperty("levelDificulty")) {
            ratio = 1 - userModel.scoreRules.levelDificulty;
        }
    }
    return newLevel = Math.round(Math.log2(newPoints * ratio));
};
//Sumarize User info
composeLeaderboard = function (tdfName, user, numElements) {
    var students = [];
    var final = [];
    var BuildedTdfName = buildTdfDBName(tdfName);
    var studentsIn = StudentsInTDF_withScores(BuildedTdfName);
    var userModel = GetUserModel(user);
    if (userModel != null) {

        var userScore = {};
        studentsIn.forEach(function (student) {
            var userModel = GetUserModel(student);
            if (userModel == null)
                return;
            student.score = GetUserPoints(student, BuildedTdfName);
            var shortname = userGetShortname(student);
            if (shortname == null)
                return;
//            var name = userGetUsername(student);
//            if (name == null)
//                return;
//            var shortname = name.slice(0, name.search("@")); //get name until @
            student.username = shortname;
            student.isTheActive = "";
            if (student._id == user._id) {
                userScore = student;
                student.isTheActive = "isTheActive";
            }
            ;
            students.push(student);
        });
        //Sort the students in descending order
        students.sort(function (a, b) {
            return b.score - a.score;
        });
        var i = 1;
        students.forEach(function (student) {
            student.position = i;
            i++;
        });
        //Slice array to number of elements to be showing
        if (students.length > numElements) {
            var position = students.findIndex(function (student) {
                return student._id == user.id_;
            });
            if (GetUserDynamicLeaderboard(user)) {

                if (position > numElements - 1) {
                    var mod = Math.round((numElements - 1) % 2);
                    var distribuition = Math.round((numElements - 1) / 2) - mod;
                    var pos = position + distribuition;
//             tam = students.length;
                    var lim = students.length - 1;
                    var resto = lim - pos
                    var cutPos = position + distribuition + 1;
                    if (resto > 0)
                        resto = 0;
                    resto *= -1;
//             corta = (- distribuition + resto)*-1;
                    var cutNeg = position - (distribuition + resto + mod);
                    var MaiorPos = students.slice(position, cutPos);
                    var MenorPos = students.slice(cutNeg, (position));
                    final = MenorPos.concat(MaiorPos);
                } else {
                    final = students.slice(0, numElements);
                }
            } else {
                if (position > numElements - 1) {
                    students.splice(position, 1);
                    students.splice(4, 0, userScore);
                }
                final = students.slice(0, numElements);
            }
        } else {
            final = students;
        }
        //students.buttonColor = determineButtonColor(student.score);
        //console.log(student);

        //console.log("buttons");
        //console.log(buttons);
        return final;
    }

};
ComposePerformanceTable = function (user, tdfname) {
    var userStats = [];
    var itemData = GetStudentAvgData(user, tdfname);
    //userStats.push({indice:"Itens Corretos",valor:itemData.ItemsCorrectness});
    //userStats.push({indice:"Latencia por Itens",valor:itemData.ItemsLatency});
    userStats.push({indice: "Total de Acertos", valor: itemData.totalCorrect});
    userStats.push({indice: "Total Respondido", valor: itemData.totalQuestions});
    userStats.push({indice: "Média de acertos (%)", valor: ((itemData.totalAvgCorrect) * 100).toFixed(2)});
    userStats.push({indice: "Tempo médio por resposta (s)", valor: (itemData.totalAvgLatency / 1000).toFixed(2)});
    //userStats.push({indice:"Média de acertos ", valor: itemData.totalAvgCorrect2});
    //userStats.push({indice:"Média de tempo entre respostas", valor: itemData.totalAvgLatency2});
    return userStats;
};
GetStudentPerItemData = function (user, tdfName) {
    //Fetch the data from the db
    var userDataQuery = {};
    var tdfname = buildTdfDBName(tdfName);
    userDataQuery[tdfname] = {$exists: true};
    var userData = UserMetrics.find({'_id': user._id}, userDataQuery).fetch();
    var itemIDList = _.chain(userData).first().prop(tdfname).safekeys().value();
    var cluster = _.chain(Stimuli.findOne({fileName: getCurrentStimName()}))
            .prop("stimuli")
            .prop("setspec")
            .prop("clusters").first()
            .prop("cluster")
            .value();
    // Get current items for associating the names with the IDs
    var itemStats = [];
    _.chain(userData).first().prop(tdfname).each(function (item) {
        var corCount = 0;
        var totCount = 0;
        var corTime = 0;
        //Iterate over the item's correctness data
        var questionCount = _.intval(item.questionCount || 0);
        for (var i = 0; i < questionCount; i++) {
            totCount++;
            if (!(_.isUndefined(item.answerCorrect)) && item.answerCorrect[i]) {
                corCount++;
                corTime += item.answerTimes[i];
            }
        }
//MONGODB_URI: heroku config:add MONGO_URL= mongodb://heroku_jx93fr7w:n2akc72evoskt2e0ue2dd1446l@ds121483.mlab.com:21483/heroku_jx93fr7w
// heroku config:add ROOT_URL=https://mofactsbr.herokuapp.com/
        var newIndex = itemStats.length;
        var itemID = itemIDList[newIndex];
        itemStats.push({
            'correctRatio': (corCount / totCount).toFixed(2),
            'avgLatency': (_.isNaN(corTime / corCount) ? 0 : corTime / corCount).toFixed(1),
            'repetitions': totCount,
            'itemID': itemID,
            'name': _.chain(cluster).prop(itemID).prop("display").first().value(),
        });
    });
    return itemStats;
};
GetStudentAvgData = function (user, tdfName) {
    var userData = UserMetrics.findOne({'_id': user._id});
    var tdfname = buildTdfDBName(tdfName);
    var itemDataLat = [];
    var itemDataCorr = [];
    var itemCount = [];
    var totalAvgCorrect = 0;
    var totalAvgLatency = 0;
    var totCorTime = 0;
    var totItensAnswered = 0;
    var totCorrCount = 0;
    _.chain(userData).prop(tdfname).each(function (item, itemIndex) {
        //Each item in the TDF
        var questionCount = _.intval(item.questionCount || 0);
        for (var i = 0; i < questionCount; i++) {
            while (itemCount.length <= i) {
                itemCount.push(0);
                itemDataLat.push(0);
                itemDataCorr.push(0);
            }
            itemCount[i]++;
            totItensAnswered++;
            if (!(_.isUndefined(item.answerCorrect)) && item.answerCorrect[i]) {
                totCorrCount++;
                //if (optionBool) {
                itemDataCorr[i]++;
                // }
                //else {
                totCorTime += item.answerTimes[i];
                itemDataLat[i] += item.answerTimes[i];
                //}
            }
        }
    });
    var totCorTime2 = 0;
    var totItensLat2 = 0;
    // Now we have the data, turn it into averages, replacing itemData's values with the averages
    for (var i = 0; i < itemDataCorr.length; i++) {
        if (itemCount[i] !== 0) { // opt bool == true correctness
            itemDataCorr[i] /= itemCount[i];
            totCorTime2 += itemDataCorr[i];
        } else {
            itemDataCorr[i] = 0;
            totCorTime2 = itemDataCorr[i];
        }
    }
    // Now we have the data, turn it into averages, replacing itemData's values with the averages
    for (var i = 0; i < itemDataLat.length; i++) {
        if (totCorrCount !== 0) {       // opt bool == false latencia
            itemDataLat[i] /= totCorrCount;
            totItensLat2 += itemDataLat[i];
        } else {
            itemDataLat[i] = 0;
            totItensLat2 = itemDataLat[i];
        }
    } // Fernando Testar essa função, ver como está ficando o retorno

    // Quick-and-dirty checking to make sure that the last element isn't because of 0 attempts made.
    if (_.last(itemCount) === 0) {
        itemDataCorr.pop();
        itemDataLat.pop();
    } else {
        totCorTime2 /= itemDataCorr.length;
        totItensLat2 /= itemDataLat.length;
    }

    totalAvgCorrect = (totItensAnswered > 0) ? totCorrCount / totItensAnswered : 0;
    totalAvgLatency = (totCorrCount > 0) ? totCorTime / totCorrCount : 0;
    var itemData = {};
    itemData.ItemsCorrectness = itemDataCorr;
    itemData.ItemsLatency = itemDataLat;
    itemData.totalCorrect = totCorrCount;
    itemData.totalAvgCorrect = totalAvgCorrect;
    itemData.totalAvgLatency = totalAvgLatency;
    itemData.totalAvgCorrect2 = totCorTime2;
    itemData.totalAvgLatency2 = totItensLat2;
    itemData.totalQuestions = totItensAnswered;
    return itemData;
};
//achievements control
verifyUserAchievementModel = function (user, tdfname) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    if (userModel != null) {
        if (userModel.hasOwnProperty("achievementBoard")) {
            if (userModel.achievementBoard) {
                Meteor.call("callCreateUserAchivementModel", user, tdfname, function (error, result) {
                    if (error) {
                        Meteor.call('debugLog', "erro in Checkin achievements user_id:" + user._id + " lesson " + tdfname);
                        Meteor.call('debugLog', error.reason);
                    } else {
//                    notifyAchievmentUnlocked(user);
                        Meteor.call('debugLog', "ok in Checkin achievements user_id:" + user._id + " lesson " + tdfname);
                        Meteor.call('debugLog', result);
                    }
                });
            } else {
                Meteor.call('debugLog', "user model doesn't require AchievmentBoard");
            }
        } else {
            Meteor.call('debugLog', "user model doesn't have AchievmentBoard");
        }
    }
};
function checkAchievements(user, tdfname) {
    if (user == null) {
        user = Meteor.user();
    }
    console.log("checkAchievements");
    var userModel = GetUserModel(user);
    if (userModel != null) {
        if (userModel.hasOwnProperty("achievementBoard")) {
            if (userModel.achievementBoard) {
                var query = {};
                query.user_id = user._id;
                query.tdf = tdfname;
                query.unlocked = false;
                Meteor.call('debugLog', query);
                var userAchievements = Achievements.find(query).fetch();
                console.log(userAchievements);
                if (userAchievements != null) {
                    var len = userAchievements.length;
                    for (var i = 0; i < len; i++) {
                        console.log(userAchievements[i]);
                        if (checkAchievementUnlocked(userAchievements[i], user, tdfname)) {
                            console.log("achievement unlocked");
                            // achievement['$push'][tdfname] = item;
                            var date = new Date();
                            var obj = {'$set': {'unlocked': true}};
                            obj['$set'].dateUnlocked = date;
                            Meteor.call("connAchievements", userAchievements[i]._id, obj, "upsert");
                            notifyAchievmentUnlocked(user, obj.title);
                            toastrconfigDefault();
                            var msg = "Nova Conquista alcançada";
                            var msgStyled = "<h2><center>" + msg + "</center></h2>";


                            toastr.success(msgStyled, "Nova conquista");
                            //UserMetrics.update({_id: userId}, {'$set': {'preUpdate': true}}, {upsert: true});
                            //Meteor.call("connAchievements",user._id,achievement,"upsert");
                        }
                        //checkVariable(userAchievements[tdfname][i].variable,
                        //      userAchievements[tdfname][i].operator, userAchievements[tdfname][i].value, user, tdfname);


                    }

                } else {
                    console.log("userAchievmentes null para a disciplina");
                    return ("Not find any achievment");
                }
            } else {
                Meteor.call('debugLog', "user model doesn't require AchievmentBoard");
            }
        } else {
            Meteor.call('debugLog', "user model doesn't have AchievmentBoard");
        }
    }
}
function checkAchievementUnlocked(achievement, user, tdfname) {
    return checkVariable(
            achievement.variable,
            achievement.operator,
            achievement.value,
            user, tdfname);
}
function checkVariable(variable, operator, value, user, tdf) {
    /*
     var itemData = GetStudentAvgData(user, tdf);
     itemData.ItemsCorrectness = itemDataCorr;
     itemData.ItemsLatency = itemDataLat;
     itemData.totalCorrect = totCorrCount;
     itemData.totalAvgCorrect = totalAvgCorrect;
     itemData.totalAvgLatency = totalAvgLatency;
     itemData.totalAvgCorrect2 = totCorTime2;
     itemData.totalAvgLatency2 = totItensLat2;
     itemData.totalQuestions = totItensAnswered;*/
//points = GetUserPoints(user, tdf)
//level = GetUserLevel(user, tdf)
//totalpoints = GetUserTotalPoints(user)
//totallevel = GetUserTotalLevel(user)

    switch (variable) {
        case "points" :
            return  checkOperator(GetUserPoints(user, tdf), operator, value);
            break;
        case "acertos" :
            var itemData = GetStudentAvgData(user, tdf);
            return  checkOperator(itemData.totalCorrect, operator, value);
            break;
        case "AvgAcertos" :
            var itemData = GetStudentAvgData(user, tdf);
            return  checkOperator(itemData.totalAvgCorrect, operator, value);
            break;
        case "latency" :
            var itemData = GetStudentAvgData(user, tdf);
            return  checkOperator(itemData.ItemsLatency, operator, value);
            break;
        case "AvgLatency" :
            var itemData = GetStudentAvgData(user, tdf);
            return  checkOperator(itemData.totalAvgLatency, operator, value);
            break;
        case "level" :
            return  checkOperator(GetUserLevel(user, tdf), operator, value);
            break;
        case "TotalLevel" :
            return  checkOperator(GetUserTotalLevel(user), operator, value);
            break;
        case "TotalPoints" :
            return  checkOperator(GetUserTotalPoints(user), operator, value);
            break;
    }
}
function checkOperator(value, operator, compareTo) {
    switch (operator) {
        case "=" :
            if (value >= compareTo) {
                return true;
            }
            break;
        case ">" :
            if (value > compareTo) {
                return true;
            }
            break;
        case "<" :
            if (value < compareTo) {
                return true;
            }
            break;
    }
    return false;
}
notifyAchievmentUnlocked = function (user, achievement) {
    if (user == null) {
        user = Meteor.user();
    }
    var query = {user_id: user._id, notification: true};
//    var query = {_id:user._id};
    var hasNotification = Achievements.findOne(query);
    var obj = {};
    if (hasNotification) {
        var number = hasNotification.number;
        number++; //        
        obj = {'$set': {'number': number}};
        Meteor.call("connAchievements", hasNotification._id, obj, "upsert");

    } else {
        obj.user_id = user._id;
        obj.notification = true;
        obj.message = "Você tem uma nova conquista"; //obj.message = {"New achievement unlocked"};
        obj.item = achievement;
        obj.number = 1;
        Meteor.call("connAchievements", obj._id, obj, "upsert");
    }
    soundAchievementUnlocked = new buzz.sound(srcAchievementUnlocked);
    soundAchievementUnlocked.play();


};
RemoveAchievmentNotification = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var query = {user_id: user._id, notification: true};
//    var query = {_id:user._id};
    var hasNotification = Achievements.findOne(query);
    if (hasNotification) {
        Meteor.call("connAchievements", hasNotification._id, hasNotification, "remove");
    } else {
        //   console.log(query);
        //     console.log(hasNotification);
    }

//    Meteor.call("connAchievements", {_id:user._id}, "remove");
};
SetGameOnDificult = function (user, tdf) {
    if (user == null) {
        user = Meteor.user();
    }
    var MinDificulty = 0.05;
    var maxTimeToLearn = 150;
    var calculated = 0;
    var lvl = GetUserLevel(user, tdf);
    if (lvl > 0)
        calculated = GetUserGameChance(user) - (lvl / (maxTimeToLearn + 1 - RollDice(maxTimeToLearn)));
    if (calculated <= MinDificulty) {
        return MinDificulty * 100;
    } else {
        return  calculated * 100;
    }
};
RollDice = function (lim) {
    return Math.floor((Math.random() * lim) + 1);
};
SimulateWaitingText = function (target) {
    var msgObj = "<div class = 'waiting' id='DivWaitingMsg'> "
            + "<h3> Aguardando a resposta do seu parceiro... </h3>"
            + "</div>";
    $(target).append(msgObj);
};
RemoveWaitingDiv = function (target) {
    $(target).remove();
}

GetNewAchievementUnlockedNotification = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var query = {user_id: user._id, notification: true};
//    var query = {_id:user._id};
    var hasNotification = Achievements.findOne(query);
    if (hasNotification) {
        return true;
    } else {
        return false;
    }
};
GetAchievementNotificationBody = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var query = {user_id: user._id, notification: true};
//    var query = {_id:user._id};
    var hasNotification = Achievements.findOne(query);
    if (hasNotification) {
        soundNewAchievement = new buzz.sound(srcNewAchievement);
        soundNewAchievement.play();
        return hasNotification;
    } else {
        return null;
    }
};
//Session Controls
startSession = function (user) {
    //Create our Session Variables
    Session.set("SessionID", undefined);
    Session.set("totalQuestions", 0);
    Session.set("totalCorrect", 0);
    Meteor.call('sessionStart', user, function (Error, result) {
        if (Error) {
            console.log('Erro ao iniciar sessão');
            console.log(Error);
        } else {
            Session.set("SessionID", result);
            console.log("Started session " + Session.get("SessionID"));
        }

    });
};
sessionRouting = function (origem, destino) {
    if ((Session.get("SessionID") == null) || (Session.get("SessionID") == ""))
        return false;
    var SessionID = Session.get("SessionID");

    var query = {};
    query._id = SessionID;

    var sessionInfo = UserSessions.findOne(query);
    if (sessionInfo != null) {
        sessionInfo.lastseen = new Date();
        if (sessionInfo.hasOwnProperty("routes")) {
            var route = {};
            sessionInfo.routes.push({"origem": origem, "destino": destino});
        } else {
            sessionInfo.routes = [{"origem": origem, "destino": destino}];
        }
        Meteor.call("connSessions", SessionID, sessionInfo, "upsert", function (Error, result) {
            if (Error) {
                console.log('Erro no registro da sessão');
                console.log(Error);
            } else {
                /* console.log("session updated");
                 console.log("session ID " + SessionID);*/
            }
        });
    }
};
//keeptrack about the user
updateSessionInfo = function (CallingToIncrease, Correct) {
    if (CallingToIncrease) {
        if (Correct) {
            var totCor = Session.get("totalCorrect") + 1;
            Session.set("totalCorrect", totCor);
        }
        var tot = Session.get("totalQuestions") + 1;
        Session.set("totalQuestions", tot);
        //console.log("Session updated: " + Session.get("totalQuestions") + Session.get("totalCorrect"));

    }
    var SessionID = Session.get("SessionID");
    var totalQuestions = Session.get("totalQuestions");
    var totalCorrect = Session.get("totalCorrect");
    /* console.log(SessionID);
     console.log(totalQuestions);
     console.log(totalCorrect);*/
    Meteor.call("updateSession", SessionID, totalQuestions, totalCorrect, function (Error, result) {
        if (Error) {
            console.log('Erro no registro da sessão');
            console.log(Error);
        } else {
            /* console.log("session updated");
             console.log("session ID " + SessionID);*/
        }
    });
};
//finish the session if he/she clicks in logout;
FinishSession = function (user, happiness, control, arousal, comment) {
    var SessionID = Session.get("SessionID");
    var totalQuestions = Session.get("totalQuestions");
    var totalCorrect = Session.get("totalCorrect");
    happiness = null ? 0 : happiness;
    control = null ? 0 : control;
    arousal = null ? 0 : arousal;
    comment = null ? "" : comment;
    /* console.log(SessionID);
     console.log(totalQuestions);
     console.log(totalCorrect);*/
    Meteor.call("sessionFinished", SessionID, totalQuestions, totalCorrect, happiness, control, arousal, comment, function (Error, result) {
        if (Error) {
            console.log('Erro no registro da sessão');
            console.log(Error);
        } else {
            /* console.log("session Finished");
             console.log("session ID " + result);
             Session.set("SessionID", undefined);
             Session.set("totalQuestions", 0);
             Session.set("totalCorrect", 0);*/

        }
    });
};
//Log interaction with gameElement into the user Model
RegisterInteraction = function (user, element) {
    console.log(Session.get("SessionID"));
    if (!user)
        return null;
    //Log into UserModel
    var userModel = GetUserModel(user);
    if (!userModel)
        return false;
    if (!userModel.hasOwnProperty("InteractionLogs")) {
        userModel.InteractionLogs = {};
    }
    if (!userModel.InteractionLogs.hasOwnProperty(element)) {
        userModel.InteractionLogs[element] = 0;
    }
    userModel.InteractionLogs[element]++;
    // console.log("userModel antes do update");
    //   console.log(userModel);
    Meteor.call("connGamification", user._id, userModel, "upsert", function (error, result) {
        if (error) {
            console.log(error);
        } else {
            //        console.log("fez o registro no modelo");
            //        console.log(userModel);
        }
    });
    //Log in the session

    if (Session.get("SessionID")) {
        var userSession = GetUserSession(Session.get("SessionID"));
        if (!userSession.hasOwnProperty("InteractionLogs")) {
            userSession.InteractionLogs = {};
        }
        if (!userSession.InteractionLogs.hasOwnProperty(element)) {
            userSession.InteractionLogs[element] = 0;
        }
        userSession.InteractionLogs[element]++;
        Meteor.call("connSessions", userSession._id, userSession, "upsert", function (error, result) {
            if (error) {
                console.log(error);
            }
        });
    }

};
//ldk:bootstrap-progressbar
RestoreUserHealth = function (user, value, type) {
    if (user == null) {
        user = Meteor.user();
    }

    var userModel = GetUserModel(user);
    if (userModel != null) {
        var maxHealth = GetUserMaxHealth(null);
        if (value == null)
            value = GetUserRecoverHealthValue(null);
        if (type == null)
            type = GetUserRecoverHealthType(null);
        if (userModel.hasOwnProperty("Health")) {
            switch (type) {
                case "%" :
                    userModel.Health *= 1 + (value / 100);
                    break;
                case "real" :
                    userModel.Health -= value;
                    break;
                default :
                    userModel.Health *= 1 + (value / 100);
                    break;
            }
            if (userModel.Health > maxHealth)
                userModel.Health = maxHealth;
            var obj = {'$set': {'Health': Math.round(userModel.Health)}};
            Meteor.call("connGamification", user._id, obj, "update");
            return userModel.Health;
        }
    }
    return false;
};
SetUserNewLife = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    if (userModel != null) {
        var maxHealth = GetUserMaxHealth();
        if (userModel.hasOwnProperty("Health")) {
            userModel.Health = maxHealth;
            var obj = {'$set': {'Health': userModel.Health}};
            Meteor.call("connGamification", user._id, obj, "update");
            return userModel.Health;
        }
    }
    return false;
};
UserTakeDamage = function (user, value, type) { //lose Health
    //var soundDamage = new buzz.sound(srcSoundDamage);
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    if (userModel != null) {
        var maxHealth = GetUserMaxHealth(null);
        if (value == null)
            value = GetUserDamageValue(null);
        if (type == null)
            type = GetUserDamageType(null);
        if (userModel.hasOwnProperty("Health")) {
            switch (type) {
                case "%" :
                    userModel.Health -= Math.floor(maxHealth * (value / 100));
                    break;
                case "real" :
                    userModel.Health -= value;
                    break;
                case "TimeOut" :
                    userModel.Health -= Math.floor(maxHealth * (GetUserDamageOnTimeOut() / 100));
                    break;
                default :
                    userModel.Health -= Math.floor(userModel.Health * (value)); //damage base on current 
                    break;
            }
            if (Math.floor(userModel.Health) <= 0)
                userModel.Health = 0;
            var obj = {'$set': {'Health': userModel.Health}};
            Meteor.call("connGamification", user._id, obj, "update");
            // soundDamage.play();
            return userModel.Health;
        }
    }
    return false;
};
GetUserShowHealth = function (user) {
    if (user == null) {
        user = Meteor.user();
    }
    var userModel = GetUserModel(user);
    if (userModel != null) {
        if (userModel.hasOwnProperty("showHealth")) {
            return userModel.Health;
        }
    }
    return false;
};

ExportSessionsData = function () {

};
ExportAllRoutes = function (format) {

    var routes = ExportRoutes();


    if (format == "json") {
        var JRoutes = JSON.stringify(routes);

        return JRoutes;
    } else if (format == "csv") {

        //console.log(Json);
        // const items = json3.items
        //var json = json3.items
        //var fields = Object.keys(routes[0]);
        var fields = getheaders(routes);
        var replacer = function (key, value) {
            return value === null ? '' : value;
        };
        var RoutesCSV = routes.map(function (row) {
            return fields.map(function (fieldName) {
                return JSON.stringify(row[fieldName], replacer);
            }).join(';');
        });
        RoutesCSV.unshift(fields.join(';')); // add header column

        RoutesCSV.join('\r\n');

        return RoutesCSV;

    } else {

        return false;
    }

};
ExportSession = function (format) {

    var sessions = ExportSessions();


    if (format == "json") {
        var JRoutes = JSON.stringify(sessions);

        return JRoutes;
    } else if (format == "csv") {

        //console.log(Json);
        // const items = json3.items
        //var json = json3.items
        //var fields = Object.keys(routes[0]);
        var fields = getheaders(sessions);
        var replacer = function (key, value) {
            return value === null ? '' : value;
        };
        var RoutesCSV = sessions.map(function (row) {
            return fields.map(function (fieldName) {
                return JSON.stringify(row[fieldName], replacer);
            }).join(';');
        });
        RoutesCSV.unshift(fields.join(';')); // add header column

        RoutesCSV.join('\r\n');

        return RoutesCSV;

    } else {

        return false;
    }

};

ExportInfo = function (format) {
    var AllUsers = [{}];

    //  console.log("user");
    var users = [];
    var count = 0;
    Meteor.users.find({}).forEach(function (val) {
        //  console.log(val);
        var user = {};
        user._id = val._id;
        user.name = userGetUsername(val);
        //if ((user.name.toString().toLowerCase() == "fernando.heb@gmail.com") || (user.name.toString().toLowerCase() == "teste@gmail.com"))        {            return;        }

        var genero = formatGender(GetUserExternProfileGenero(val));
        //  if (genero>0)
        user.genero = GetUserExternProfileGenero(val);
        user["genero-formated"] = genero;
        var idade = GetUserExternProfileIdade(val);
        if (idade)
            user.idade = idade;


        var profile = formatProfiles(GetUserProfileName(val));
        //if (profile)
        user.profileName = GetUserProfileName(val);
        user["profileName-formated"] = profile;
//        else
        //          user.profileName = "none";

        var extProfile = formatProfiles(GetUserExternProfileName(val));
        //if (extProfile)
        user.ExternProfile = GetUserExternProfileName(val);
        user["ExternProfile-formated"] = extProfile;
        //else        //  user.ExternProfile = "none";



        var group = formatExpGroup(GetExperimentalGroup(val));
        //  if (!group)
        //      group = "none";

        user.ExperimentalGroup = GetExperimentalGroup(val);
        user["ExperimentalGroup-Formated"] = group;


        /*var microGamified = GetUserIsMicroGamified(val);
         if (microGamified)
         user.Microgamified = GetUserIsMicroGamified(val);
         else
         user.Microgamified = "false";*/
        var sess = SessionsDataSummary(val);
        if (sess) {
            for (var i in sess) {
                //     console.log(i);

                user[i] = sess[i];

            }
        }
        var logs = ExportInteractionLog(val);
        if (logs) {
            for (var i in logs) {
                // logs[i].key;
                //user["LOG-" + logs[i].key] = logs[i].val;

                user[i] = logs[i];


            }
        }
        var fields = ExportExternProfileFields(val);
        if (fields) {
            for (var i in fields) {
                //  console.log(i);
                if (i == "genero") {
                    user[i + "-formated"] = formatGender(fields[i]);
                    user[i] = fields[i];
                } else if (i == "escolaridade") {
                    user[i] = fields[i];
                    user[i + "-formated"] = formatScholarship(fields[i]);
                } else
                    user[i] = fields[i];
            }
        }
        /**grouped**/
        var sess = SessionsDataSummary(val);
        if (sess) {
            for (var i in sess) {
                //     console.log(i);
//                user["macro_" + i] = null;
//                user["micro_" + i] = null;
//                user["standard_" + i] = null;
//                user["none_" + i] = null;
//                user["1_" + i] = null;
//                user["2_" + i] = null;
//                user["3_" + i] = null;
//                user["0_" + i] = null;
//                user[group + "_" + i] = sess[i];
                user[i] = sess[i];

            }
        }

        /****/
        // user.CountSessions = GetNumberOfSessions(val);
        users[count] = user;
        count++;
        // console.log(user);

    });
    if (format == "json") {
        var JUser = JSON.stringify(users);
        return JUser;
    } else if (format == "csv") {

        //console.log(Json);
        // const items = json3.items
        //var json = json3.items
        //  var fields = Object.keys(users[0]);
        /* Find a way to put all the fields here...*/

        var fields = getheaders(users);
        //    console.log(fields);

        var replacer = function (key, value) {
            return value === null ? '' : value;
        };
        var UsersCSV = users.map(function (row) {
            return fields.map(function (fieldName) {
                return JSON.stringify(row[fieldName], replacer);
            }).join(';');
        });
        UsersCSV.unshift(fields.join(';')); // add header column

        UsersCSV.join('\r\n');

        return  UsersCSV;

    } else {

        return false;
    }


    /*
     
     
     console.log("UserProfileData");
     UserProfileData.find({}).forEach(function (val) {
     console.log(val);
     });
     
     console.log("Stimuli");
     Stimuli.find({}).forEach(function (val) {
     console.log(val);
     });
     console.log("Tdfs");
     Tdfs.find({}).forEach(function (val) {
     console.log(val);
     });
     console.log("UserTimesLog");
     UserTimesLog.find({}).forEach(function (val) {
     console.log(val);
     });
     
     console.log("UserMetrics");
     UserMetrics.find({}).forEach(function (val) {
     console.log(val);
     });
     /* consolel.og(ScheduledTurkMessages.find({}).forEach(function (val) {
     console.log(val);
     }));*/

//Fernando's Collection
    /*
     console.log("Gamification");
     Gamification.find({}).forEach(function (val) {
     console.log(val);
     });
     console.log("Scores");
     Scores.find({}).forEach(function (val) {
     console.log(val);
     });
     console.log("AchievementModels");
     AchievementModels.find({}).forEach(function (val) {
     console.log(val);
     });
     console.log("Achievements");
     Achievements.find({}).forEach(function (val) {
     console.log(val);
     });
     
     console.log( "UserSessions");
     UserSessions.find({}).forEach(function (val) {
     console.log(val);
     });
     console.log("ExperimentData");
     ExperimentData.find({}).forEach(function (val) {
     console.log(val);
     });
     */

};


getheaders = function (obj) {
    var arr = {};
    _.chain(obj)
            .map(function (attr) {
                return _.keys(attr).map(function (key) {
                    arr[key] = null;
                    return null;
                });
            });
    var keys = Object.keys(arr);
    //console.log(keys);
    return keys;
};
//gender
formatGender = function (val) {
    switch (val) {
        case "m" : //male
            return 1;
            break;
        case "f" : //female
            return 2;
            break;
        case "o" : //other
            return 3;
            break;
        default : // "n"  not informing
            return -1;
            break;
    }
};

formatScholarship = function (val) {
    // val = val.trim();
    switch (val) {
        case "Ensino Fundamental Incompleto" : //male
            return 0;
            break;
        case "Ensino Fundamental Completo" : //female
            return 1;
            break;
        case "Ensino Médio Incompleto":
        case "Ensino MÃ©dio Incompleto" : //other
            return 2;
            break;
        case "Ensino Médio Completo":
        case "Ensino MÃ©dio Completo" : //male
            return 3;
            break;
        case "Ensino Superior Incompleto" : //female
            return 4;
            break;
        case "Ensino Superior Completo" : //other
            return 5;
            break;
        case "Pós-Graduação Incompleta":
        case "PÃ³s-GraduaÃ§Ã£o Incompleta" : //other
            return 6;
            break;
        case "Pós-Graduação Completa":
        case "PÃ³s-GraduaÃ§Ã£o Completa" : //other
            return 7;
            break;
        default : // "n"  not informing
            return -1;
            break;
    }
    /*
     <option value="Ensino Fundamental Incompleto">Ensino Fundamental Incompleto</option>
     <option value="Ensino Fundamental Completo">Ensino Fundamental Completo</option>
     <option value="Ensino Médio Incompleto">Ensino Médio Incompleto</option>
     <option value="Ensino Médio Completo">Ensino Médio Completo</option>
     <option value="Ensino Superior Incompleto">Ensino Superior Incompleto</option>
     <option value="Ensino Superior Completo">Ensino Superior Completo</option>
     <option value="Pós-Graduação Incompleta">Pós-Graduação Incompleta</option>
     <option value="Pós-Graduação Completa">Pós-Graduação Completa</option> 
     */
};

formatExpGroup = function (val) {
    switch (val) {
        case "macro" :
            return 1;
            break;
        case "micro" :
            return 2;
            break;
        case "standard" :
            return 3;
            break;
        default :
            return 0;
            break;
    }
};
createNullFields = function (field, value, fill) {
    var obj = {};
    //var groups = ["macro", "micro", "standard"];
    var groups = ["1", "2", "3"];
    for (var i in groups) {
        obj[groups[i] + "_" + field] = value;
    }

    return fill(obj);

};

formatProfiles = function (val) {
    switch (val) {
        case "Realizacao" :
        case "Achiever":
            return 1;
            break;
        case "Social" :
        case "Socializer" :
            return 2;
            break;
        case "Imersao" :
        case "Imersive" :
            return 3;
            break;
        case "Empate" :
        case "Standard":
            return 0;
            break;
        default :
            return -1;
            break;
    }
};

/*
 UserScore = function (user, tdfname) {
 var indivUserQuery = {'_id': user._id};
 //console.log("compute score user ");
 //console.log(indivUserQuery);
 // We use findOne because there should only ever be one user with any given id.
 var indivUser = UserMetrics.findOne(indivUserQuery);
 //console.log(indivUser);
 
 var score = [];
 if (tdfname != null) {
 var tdf = buildTdfDBName(tdfname);
 var tdfData = {};
 tdfData.askCount = 0;
 tdfData.correctCount = 0;
 tdfData.tdfName = tdf;
 _.chain(indivUser).prop(tdf).each(function (item) {
 tdfData.askCount += _.chain(item).prop('questionCount').intval().value();
 tdfData.correctCount += _.chain(item).prop('correctAnswerCount').intval().value();
 });
 score.push(tdfData);
 } else {
 var tdfs = userAnsweredTDFS(user);
 //var count = 0;
 tdfs.forEach(function (tdf) {
 var tdfData = {};
 tdfData.askCount = 0;
 tdfData.correctCount = 0;
 _.chain(indivUser).prop(tdf).each(function (item) {
 tdfData.tdfName = tdf;
 tdfData.askCount += _.chain(item).prop('questionCount').intval().value();
 tdfData.correctCount += _.chain(item).prop('correctAnswerCount').intval().value();
 });
 score.push(tdfData);
 //count++;
 });
 }
 
 //var tryValues = getCurrentScoreValues();
 //console.log( "try calues "+Values[0]);
 return score; // Note the possible of DBZ which would return NaN
 };
 UserGetScore = function (user, tdfname) {
 var userModel = GetUserModel(user);
 if (userModel == null)
 return 0;
 var ratio = 0.8; //defaultCase 1-0.2
 if (userModel.hasOwnProperty("scoreRules")) {
 if (userModel.scoreRules.hasOwnProperty("ratio")) {
 ratio = 1 - userModel.scoreRules.ratio;
 }
 }
 
 var score = UserScore(user, tdfname);
 //var point = score[0].correctCount * ratio; //Math.round(Math.pow(score[0].correctCount,2)/score.askCount);
 //return point;           
 var totalAsked = 0;
 var totalCorrect = 0;
 score.forEach(function (res) {
 totalAsked += res.askCount;
 totalCorrect += res.correctCount;
 });
 var point = totalCorrect * ratio; //Math.round(Math.pow(totalCorrect,2)/totalAsked);
 return Math.round(point);
 };
 UserGetTotalScore = function (user) {
 var userModel = GetUserModel(user);
 if (userModel == null)
 return 0;
 
 var ratio = 0.8; //defaultCase 1-0.2
 if (userModel.hasOwnProperty("scoreRules")) {
 if (userModel.scoreRules.hasOwnProperty("ratio")) {
 ratio = 1 - userModel.scoreRules.ratio;
 }
 }
 var score = UserScore(user);
 var totalAsked = 0;
 var totalCorrect = 0;
 score.forEach(function (res) {
 totalAsked += res.askCount;
 totalCorrect += res.correctCount;
 });
 var point = totalCorrect * ratio; //Math.round(Math.pow(totalCorrect,2)/totalAsked);
 return Math.round(point);
 };
 UserGettGamifictionAllStatus = function (user) {
 var userProfile = {};
 userProfile = Gamification.findOne({_id: user._id});
 };
 */
/*composeLeaderboard = function (tdfName) { // old 
 var students = [];
 var BuildedTdfName = buildTdfDBName(tdfName);
 var studentsIn = StudentsInTDF(BuildedTdfName);
 studentsIn.forEach(function (student) {
 var userModel = GetUserModel(student);
 if (userModel == null)
 return;
 student.score = UserGetScore(student, BuildedTdfName);
 student.username = userGetUsername(student);
 students.push(student);
 });
 
 //students.buttonColor = determineButtonColor(student.score);
 //console.log(student);
 
 //console.log("buttons");
 //console.log(buttons);
 return students;
 };*/

//GameUser = function () {
//    console.log("called");
//    var meteorUser = Meteor.user();
//    var perfil = GetUserModel(meteorUser);
//    var troca = function () {
//        var obj = {'$set': {'leaderboard': !GetUserShowLeaderBoard(user),
//                'personalStatus': !GetUserShowPersonalStatus(user)}};
//        Meteor.call("connGamification", user._id, obj, "update");
//    };
//    var refresh = function(){
//        perfil = GetUserModel(meteorUser);
//    };
//};