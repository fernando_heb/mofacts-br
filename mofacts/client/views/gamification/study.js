////////////////////////////////////////////////////////////////////////////
// Local collection for buttons

var progressButtons = new Mongo.Collection(null);
var selectedTDF = null;

////////////////////////////////////////////////////////////////////////////
// Template storage and helpers
Template.study.onRendered(function () {

});

Template.study.helpers({
    username: function () {
        if (!haveMeteorUser()) {
            sessionRouting("study","signin");
            routeToSignin();
        } else {
            return Meteor.user().username;
        }
    },
    tdfs: function () {
        var obj = [{}];
        var i = 0;
        Tdfs.find({}).forEach(function (tdfObject) {
            //Make sure we have a valid TDF (with a setspec)

            var setspec = _.chain(tdfObject)
                    .prop("tdfs")
                    .prop("tutor")
                    .prop("setspec").first()
                    .value();

            /* if (!setspec) {
             console.log("Invalid TDF - it will never work", tdfObject);
             return;
             }
             */
            var tdf = tdfObject.fileName;
            var name = _.chain(setspec).prop("lessonname").first().value();
            // console.log(name);
            // var stimulusFile = _.chain(setspec).prop("stimulusfile").first().value();

            var custo = getCusto(GetUserLevel(null, tdf));
            var custoIco = "$$";

          
            

            if (setspec.userselect) {
                if (i == 0) {
                    obj[i].fileName = tdf;
                    //  obj[i].value = stimulusFile;
                    obj[i].description = name;
                    obj[i].custo = custo;
                    obj[i].custoIco = custoIco;
                    i++;
                } else {
                    obj.push({//'value': stimulusFile,
                        'description': name,
                        'fileName': tdf,
                        'custo': custo,
                        'custoIco': custoIco
                    });
                }
            }

        });
        return obj;
    },
    contentLesson: function () {
        var tdf = Session.get("selectedTDF");
        if ((!tdf) || (tdf == "null"))
            return false;
 
        var obj = [{}];
        var tdfObject = Tdfs.findOne({fileName: tdf});
        //Make sure we have a valid TDF (with a setspec)

        var cluster = null;
        _.chain(tdfObject)
                .prop("tdfs")
                .prop("tutor")
                .prop("unit").each(function (unit) {
            _.chain(unit).prop("learningsession").each(
                    function (learningses) {
                        cluster = _.chain(learningses).prop("clusterlist").first().value();
                    });
        });
        var begin = cluster.slice(0, cluster.search("-"));
        ;
        var ends = cluster.slice(cluster.search("-") + 1, cluster.toString().length);
        ;

        var setspec = _.chain(tdfObject)
                .prop("tdfs")
                .prop("tutor")
                .prop("setspec").first()
                .value();

        var stimulusFile = _.chain(setspec).prop("stimulusfile").first().value();
//        var stimulusFile = null;
//        stimulusFile = _.chain(tdfObject)
//                .prop("tdfs")
//                .prop("tutor")
//                .prop("setspec")
//                .prop("stimulusfile").first().value();
//        

        var cardObj = [{}];
        var i = 0;
        var inserted = 0;
        var stim = Stimuli.findOne({'fileName': stimulusFile});
        _.chain(stim).prop("stimuli").prop("setspec").prop("clusters").each(function (clusters) {
            _.chain(clusters).prop("cluster").each(function (item) {
                if ((i >= begin) && (i <= ends)) {
                    var obj = [{}];
                    if (item.hasOwnProperty("display"))
                        obj.question = item.display;
                    else
                        obj.question = null;
                    if (item.hasOwnProperty("response"))
                        obj.answer = item.response;
                    else
                        obj.answer = null;
                    if ((obj.question !== null) && (obj.answer !== null)) {
                        var cardnumber = "";
                        if (inserted % 2 == 0)
                            cardnumber = "CardPar";
                        else
                            cardnumber = "CardImpar";
                        cardObj.push({'question': obj.question, 'answer': obj.answer, 'cardNumber': cardnumber});
                        inserted++;
                    }
                }
                i++;
            });
        });

        cardObj.shift();


        return cardObj;

    },
    'formatDivs': function () {
        var obj = {};

        obj.cardQuestion = "col-xs-6";
        obj.cardAnswer = "col-xs-6";


        return obj;
    },
});

////////////////////////////////////////////////////////////////////////////
// Template Events
function chargeLesson(tdf){        
    if (tdf == null  || tdf == "null")
        return false;
    
     var lvl = GetUserLevel(null, tdf);
            var custo = getCusto(lvl);
            var lowPointsMsg= "Opa..Você não tem pontos suficientes para consultar o material,\n tente ganhar alguns pontos e volte novamente."
        if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
            return false;
        else
            return true;    
}

Template.study.events({
//    
    'change #tdfSelector': function () {
        // $("#tdfSelector").change(function(){
        var selected = $("#tdfSelector").find(':selected').val();
        if (chargeLesson(selected))
            Session.set("selectedTDF", selected);
        else
            $('div.select-lesson select').val("null");
        //});
       
    },
    "switchChange.bootstrapSwitch [name='switch']": function (event, data) {
        //console.log(data);
        var status = data === true ? 'on' : 'off';
       // Meteor.call('toggle', status);


    },
});

function getCusto(lvl) {
    if (lvl > 5)
        return lvl * 50;

    return 0;

};
