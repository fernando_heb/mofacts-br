////////////////////////////////////////////////////////////////////////////
// Template storage and helpers

//INPUT: user, which is an object containing an _id which corresponds to a doc
//       in UserMetrics, and the name of the relevant Tdf (in Mongo-recognizable
//       format)
//OUTPUT: a ratio which is the user's average score across all items for the
//        client's current system.

//  var user = new Mongo.Collection(null); 
/*
function computeUserScore(user, tdfname) {
    var indivUserQuery = {'_id': user._id};
    //console.log("compute score user ");
    //console.log(indivUserQuery);
    // We use findOne because there should only ever be one user with any given id.
    var indivUser = UserMetrics.findOne(indivUserQuery);
    //console.log(indivUser);
    var askCount = 0;
    var correctCount = 0;




    _.chain(indivUser).prop(tdfname).each(function (item) {
        askCount += _.chain(item).prop('questionCount').intval().value();
        correctCount += _.chain(item).prop('correctAnswerCount').intval().value();
    });
    // var tryValues = getCurrentScoreValues();
   //console.log( "try calues "+Values[0]);
    return correctCount / 1;  // Note the possible of DBZ which would return NaN
    /*
     * 
     * getCurrentTdfFile()
     * 
     * 
     * 
     * 
     * 
     
}

function computeTotalUserScore(user) {
    var indivUserQuery = {'_id': user._id};

    // We use findOne because there should only ever be one user with any given id.
    var indivUser = UserMetrics.findOne(indivUserQuery);
    var askCount = 0;
    var correctCount = 0;
    //var tdfs[]
    //foreach(tdfname in tdfs) {
    _.chain(indivUser).prop(tdfname).each(function (item) {
        askCount += _.chain(item).prop('questionCount').intval().value();
        correctCount += _.chain(item).prop('correctAnswerCount').intval().value();
    });
    //}
    return correctCount / askCount;  // Note the possible of DBZ which would return NaN
}
*/
Template.leaderBoard.helpers({
    username: function () {
        if (!haveMeteorUser()) {
            routeToSignin();
        } else {
            return Meteor.user().username;
        }
    },
    students: function () {
      /*  var buttons = [];
///// student = StudentsInTDF(getCurrentTdfName())
        var userQuery = {};
        //var tdfFile = 'HiraganaOptim.xml';
        //console.log("current " + getCurrentTdfName());
        var tdfDBName = buildTdfDBName(getCurrentTdfName());
       // var tdfFile = tdfDBName;
        // console.log("TDFNAME   "+ tdfname);
       // var tdfDBName = buildTdfDBName(tdfFile);
        //   console.log("tdfDBNAme  " + tdfDBName);
        //        console.log(userQuery[tdfDBName] = {'$exists': true});
        userQuery[tdfDBName] = {'$exists': true};
        //console.log("userQuery");
        //console.log(userQuery);
        //console.log("userQuery TdfDBname");
        //console.log(userQuery[tdfDBName]);
        
/////////////////
        UserMetrics.find(userQuery).forEach(function (student) {
          //  console.log("student full");
            //console.log(student);
            //console.log("student id " + student._id);
            
            
           /*
            student.forEach(function (student) {
                
                
            });*/
            /*
            Meteor.users.find({_id: student._id, "username": {$exists: true}}).forEach(function (user) {
               
              //  console.log("user");
                //console.log(user);
                
                student.username = user.username;
            }); 
            
            
                 

            
            student.score = UserGetScore(student, tdfDBName);
            student.buttonColor = determineButtonColor(student.score);
            //console.log(student);
            buttons.push(student);
        });
        //console.log("buttons");
        //console.log(buttons);
        return buttons;*/
        return composeLeaderboard(getCurrentTdfName(),Meteor.user(),5);
    }

});


////////////////////////////////////////////////////////////////////////////
// Template Events

Template.leaderBoard.events({
    'click .leaderboard': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(),"leaderboard");
    },
});


Template.leaderBoard.rendered = function () {
    // No longer used - see version history for previous code

};


