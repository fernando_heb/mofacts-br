////////////////////////////////////////////////////////////////////////////
// Local collection for buttons
var custo = 100;
var lowPointsMsg= "Humm.. Que Pena você não tem pontos suficientes para isso no momento";
var progressButtons = new Mongo.Collection(null);

////////////////////////////////////////////////////////////////////////////
// Template storage and helpers
Template.gamification.onRendered(function () {

});

Template.gamification.helpers({
    username: function () {
        if (!haveMeteorUser()) {
            sessionRouting("gamification","signin");
            routeToSignin();
        } else {
            return Meteor.user().username;
        }
    },
    
    information:function(){
        return ExportInfo();
    },
    'custo':function(){
        var obj ={};
        obj.value = custo;
//        obj.ico ="$$";
        obj.ico ="fa fa-dollar";
      return obj;  
    },
    'progressButtons': function () {
        return null;
        progressButtons.find({'temp': 1}, {'sort': {'name': 1}});
        progressButtons.remove({'temp': 1});
        /*
         //In experiment mode, they may be forced to a single tdf
         var experimentTarget = null;
         if (Session.get("loginMode") === "experiment") {
         experimentTarget = Session.get("experimentTarget");
         if (experimentTarget)
         experimentTarget = experimentTarget.toLowerCase();
         }
         
         //Will be populated if we find an experimental target to jump to
         var foundExpTarget = null;
         
         //Display different for admins/teachers
         var isAdmin = Roles.userIsInRole(Meteor.user(), ["admin", "teacher"]);*/

        //Check all the valid TDF's
        Tdfs.find({}).forEach(function (tdfObject) {
            //Make sure we have a valid TDF (with a setspec)

            var setspec = _.chain(tdfObject)
                    .prop("tdfs")
                    .prop("tutor")
                    .prop("setspec").first()
                    .value();

            /* if (!setspec) {
             console.log("Invalid TDF - it will never work", tdfObject);
             return;
             }
             */
            var name = _.chain(setspec).prop("lessonname").first().value();
            // console.log(name);
            var stimulusFile = _.chain(setspec).prop("stimulusfile").first().value();
            //console.log(stimulusFile);
            /*  if (!name) {
             console.log("Skipping TDF with no name", setspec);
             return;
             }
             
             
             
             //Check to see if we have found a selected experiment target
             if (experimentTarget && !foundExpTarget) {
             var tdfExperimentTarget = _.chain(setspec)
             .prop("experimentTarget").first().trim()
             .value().toLowerCase();
             
             if (tdfExperimentTarget && experimentTarget == tdfExperimentTarget) {
             foundExpTarget = {
             tdfkey: tdfObject._id,
             lessonName: name,
             stimulusfile: stimulusFile,
             tdffilename: tdfObject.fileName,
             how: "Auto-selected by experiment target " + experimentTarget
             };
             }
             }
             
             if (isAdmin) {
             //Admins see all TDF's, but we hide "root" TDF's
             var conditionCount = _.chain(setspec).prop("condition").prop("length").intval().value();
             if (conditionCount > 0) {
             console.log("Skipping due to condition.length > 0 (root TDF) for", name);
             return;
             }
             }
             else {
             //Non-admins (and non-teachers) can only see progress for user
             //selectable TDF's (TDF's they could have clicked on)
             //Note that we defer checking for userselect in case something above
             //(e.g. experimentTarget) auto-selects the TDF
             var userselectText = _.chain(setspec)
             .prop("userselect").first().trim()
             .value().toLowerCase();
             
             var userselect = true;
             if (userselectText === "false")
             userselect = false;
             
             if (!userselect) {
             console.log("Skipping due to userselect=false for", name);
             return;
             }
             }*/

            progressButtons.insert({
                'temp': 1,
                'name': name,
                'lessonname': name,
                'stimulusfile': stimulusFile,
                'tdfkey': tdfObject._id,
                'tdffilename': tdfObject.fileName
            });



        });
        // console.log(progressButtons.find({}));
        return progressButtons.find({});

    },
    'profile': function () {
        //var profile = GetUserModel(Meteor.user());
        var user =Meteor.user();
        var obj = {};
       // obj['achievmentsTrue'] = GetUserAchievements(user, null, true);

       // obj['achievmentsFalse'] = GetUserAchievements(user, null, false);

        obj['gameOn'] = GetUserGameOn(user);

        obj['showScore'] = GetUserShowScore(user);

        obj['showLeaderboard'] = GetUserShowLeaderBoard(user);

        obj['showPersonalStatus'] = GetUserShowPersonalStatus(user);

        obj['showAchievementBoard'] = GetUserShowAchievementBoard(user);

        obj['showLevel'] = GetUserShowLevel(user);

        obj['showHasOption'] = GetUserHasOption(user);

        obj['showHealth'] = GetUserShowHealth(user);
        
        obj['showCallChalange'] = GetUserShowCallChalange(user);
        
        
        
        return obj;
    },
    'DesafioText': function () {
        var perfil = GetUserProfileName(Meteor.user());
        var texts = {};       
        switch (perfil) {
            case "Socializer": //Friendly Design
                texts.kind = "socializer";
                texts.btnSuggest = "Quer Poder Criar Novas Metas Para Todos";            
                texts.btnAchievement = " Quer Acompanhar as metas conquistadas?";            
                texts.social = true;
                break;
            case "Achiever": //Competitivo
                texts.kind = "achiever";
                texts.btnSuggest = "Quer Poder Criar Novos Desafios Para Todos";
                texts.btnAchievement = " Quer Acompanhar os Desafios Conquistados?";                          
                break;
            default: //Imersivo, Customização
                texts.kind = "default";
                texts.btnSuggest = "Quer Poder Criar Novos Desafios Para Todos";
                texts.btnAchievement = " Quer Acompanhar os Desafios Conquistados?";                          
                break;
        }
        return texts;
    },
//    'achievmentsTrue': function () {
//        return GetUserAchievements(Meteor.user(), null, true);
// },
        /**
         Achievment_id 
         dateUnlocked
         description
         icoUrl
         operator
         tdf
         title
         unlocked
         user_id
         value
         variable
         _id
         */
   
//    'achievmentsFalse': function () {
//        return GetUserAchievements(Meteor.user(), null, false);
//
//    },
//    'gameOn': function () {
//        return GetUserGameOn(Meteor.user());
//    },
//    'showScore': function () {
//        return GetUserShowScore(Meteor.user());
//    },
//    'showLeaderboard': function () {
//        return GetUserShowLeaderBoard(Meteor.user());
//    },
//    'showPersonalStatus': function () {
//        return GetUserShowPersonalStatus(Meteor.user());
//    },
//    'showAchievementBoard': function () {
//        return GetUserShowAchievementBoard(Meteor.user());
//    },
//    'showLevel': function () {
//        return GetUserShowLevel(Meteor.user());
//    },
//    'showHasOption': function () {
//        return GetUserHasOption(Meteor.user());
//    },
//    'showHealth': function () {
//        return GetUserShowHealth(Meteor.user());
//    },
});

////////////////////////////////////////////////////////////////////////////
// Template Events

Template.gamification.events({
    
    'click .btnjson': function (event) {
        event.preventDefault();
//        console.log("getjson click");
//         var user = Meteor.user();
//         console.log(user);
//           var query = {user_id:user._id,notification:true};
////           var query = {_id:user._id};
//           console.log(query);
//    var hasNotification = Achievements.findOne(query);
//    console.log(hasNotification);
      //  Router.go("/createAchievements");
     // sessionRouting("btnjson", "AchievementBoard"); 



//       
//        
//        console.log(Session.get("SessionID"));
//        //var s = Session.get("SessionID");
//        click(Meteor.user());    
//        var user = new GameUser();

   // ExportInfo();
    var obj = {};
    obj = UserSessions.findOne({"userId":Meteor.user()._id});
       console.log(Object.keys(obj));
    
    },
    'click .switchGameOn': function (event) {
        event.preventDefault();
         
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
            
        SwitchUserGameOn(Meteor.user());
    },
    'click .switchUserScore': function (event) {
        event.preventDefault();
        
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
        SwitchUserScore(Meteor.user());
    },
    'click .switchUserLeaderboard': function (event) {
        event.preventDefault();
         
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
            
        SwitchUserLeaderboard(Meteor.user());

        if (!((GetUserShowLeaderBoard()) && (GetUserShowPersonalStatus())) && (GetUserHasOption()))
            SwitchUserHasOption(Meteor.user());
        else if (((GetUserShowLeaderBoard()) && (GetUserShowPersonalStatus())) && !(GetUserHasOption()))
            SwitchUserHasOption(Meteor.user());

    },
    'click .switchUserPersonalStatus': function (event) {
        event.preventDefault();
         
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
            
        SwitchUserPersonalStatus(Meteor.user());
        if (!((GetUserShowLeaderBoard()) && (GetUserShowPersonalStatus())) && (GetUserHasOption()))
            SwitchUserHasOption(Meteor.user());
        else if (((GetUserShowLeaderBoard()) && (GetUserShowPersonalStatus())) && !(GetUserHasOption()))
            SwitchUserHasOption(Meteor.user());
    },
    'click .switchUserAchievementBoard': function (event) {
        event.preventDefault();
         
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
            
        SwitchUserAchievementBoard(Meteor.user());
    },
    'click .switchUserLevel': function (event) {
        event.preventDefault();
         
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
            
        SwitchUserLevel(Meteor.user());
    },
    'click .switchUserHasOption': function (event) {
        event.preventDefault();
         
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
            
        SwitchUserHasOption(Meteor.user());
    },
    'click .switchShowHealth': function (event) {
        event.preventDefault();
         
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
            
        SwitchShowHealth(Meteor.user());
    },
    'click .switchShowCallChalange': function (event) {
        event.preventDefault();
         
            if(ExpendPoints(null,custo,null,lowPointsMsg) === false)
                return false;
            
        SwitchUserShowCallChalange(Meteor.user());
    },
    
    
    
    
    //console.log(GetStudentAvgData(Meteor.user(),"HiraganaOptim.xml"));
    //console.log(  GetUserAchievements(Meteor.user(),"HiraganaOptim.xml",true));
    //var VusuarioAlvo = user.email;

    /* Meteor.call('getUserProfile',usuarioAlvo, bd, function(result){
     console.log("meteorCall call back");
     console.log(result);         
     });*/
    //getUserProfile: function (function callback) {
//console.log(Meteor.user());
    /*     console.log(usersAvailableTDFS());
     console.log(userAnsweredTDFS(Meteor.user()));
     console.log(UserGetTotalScore(Meteor.user()));
     console.log(composeLeaderboard("HiraganaOptim.xml"));*/
    //    verificaModeloUsuario(Meteor.user());
    // verificaModeloUsuario(Meteor.user()); // fernando aqui testar pq não está encontrando o perfil

    //  console.log(IncreaseUserScore(Meteor.user(),"HiraganaOptim.xml"));

    //console.log(json);


    "switchChange.bootstrapSwitch [name='switch']": function (event, data) {
        console.log(data);
        var status = data === true ? 'on' : 'off';
        Meteor.call('toggle', status);


    },
});

//Same logic used in the profile template, except when the button is clicked,
//it passes all of the information to the next items page

Template.gamification.rendered = function () {
    //this is called whenever the template is rendered.


};
/*
 function verificaModeloUsuario(user) { //check User Profile
 var userModel = Gamification.findOne({_id: user._id});
 if (!userModel == null) {
 var userScore = Scores.findOne({_id: user._id});
 console.log(userModel);
 console.log(userScore);
 return userModel;
 } else {
 verificaRespostaPerfil(user);
 }
 
 }
 */
/*
 function verificaRespostaPerfil(user) {
 console.log(user.email);
 var varUserTarget = user.email;
 
 var url = 'http://acessopesquisa.caed-lab.com/gpmAPI.php'; //API address
 //     var url = 'http://jsonplaceholder.typicode.com/posts';
 //'http://localhost/git/GrupoDePesquisaManager/gpmAPI.php';
 var varAction = "mofacts";  //Action recquired from API
 var varSigla = "CAEDLAB"; //ID
 var varPassword = "123lab"; //Password
 var varGroupCode = "CAEDLAB2"; //User group
 var varDatabaseName = "ambos"; //Database name: qpjbr  - experimental - ambos
 //http://acessopesquisa.caed-lab.com/gpmAPI.php?action=exportaUsuario&sigla=CAEDLAB&senha=123lab&usuarioAlvo=teste@gmail.com&codgrp=CAEDLAB2&db=qpjbr
 //http:localhost/git/GrupoDePesquisaManager/gpmApi.php?action=exportaUsuario&sigla=CAEDLAB&senha=123lab&usuarioAlvo=fernando.heb@gmail.com&codgrp=CAEDLAB1
 //http:localhost/git/GrupoDePesquisaManager/gpmAPI.php?action = 'exportaUsuario' & sigla = 'CAEDLAB' & senha = '123lab' & codgrp = 'CAEDLAB1' & bd= ''&usuarioAlvo=fernando.heb@gmail.com&
 //var vurl = url + Vaction + Vsigla + Vsenha +VusuarioAlvo+ Vcodgrp;
 //console.log(vurl);
 console.log("meteor Call bloco try");
 var result = Meteor.http.call('GET', url, {
 // http:acessopesquisa.caed-lab.com/gpmAPI.php?
 params: {
 action: varAction,
 sigla: varSigla,
 senha: varPassword,
 usuarioAlvo: varUserTarget,
 codgrp: varGroupCode,
 db: varDatabaseName
 },
 // headers:{
 //  application/jsonrequest
 //  }
 //
 }, function (error, result) {
 if (!error) {
 console.log("callback docallback");
 console.log(result);
 var JsonRes = JSON.parse(result.content)[0];
 console.log(JsonRes);
 
 if (!JsonRes.hasOwnProperty("erro")) {
 
 return ;
 //  return  createUserGamificationModel(user, JsonRes); //trocar por função callback
 
 
 } else {
 return (console.log(JsonRes.erro + "  " + JsonRes.msg));
 }
 } else {
 return console.log(error);
 }
 });
 }
 */
/*
 function createUserGamificationModel(user, profile) {
 console.log(profile["majoritario"]);
 createGamificationReferenceModels();
 console.log(user);
 var obj = {};
 console.log(obj);
 switch (profile["majoritario"]) {
 case "Realizacao" :
 obj = Gamification.findOne({"name": "Modelo-achievement"});
 break;
 case "Social"    :
 obj = Gamification.findOne({"name": "Modelo-social"});
 break;
 case "Imersao"    :
 obj = Gamification.findOne({"name": "Modelo-imersion"});
 break;
 default :
 obj = Gamification.findOne({"name": "Modelo-draw"});
 break;
 }
 /*****
 if (profile["realizacao"] > 0){
 obj.leaderboard = true;
 }
 if (profile["social"] > 0) {
 
 }
 if (profile["imersao"] > 0) {
 
 }
 if (profile["majoritario"] > 0) {
 
 }
 ******/
/*****
 if (profile["mecanica"] > 0) {
 }
 if (profile["competicao"] > 0) {
 }
 
 if (profile["avanco"] > 0) {
 }
 if (profile["relacionamento"] > 0) {
 }
 if (profile["trabalhoemequipe"] > 0) {
 }
 if (profile["roleplaying"] > 0) {
 }
 if (profile["customizacao"] > 0) {
 }
 if (profile["socializacao"] > 0) {
 }
 
 if (profile["descoberta"] > 0) {
 }
 if (profile["escapismo"] > 0) {
 }    
 //   * @type type      
 
 obj["_id"] = user._id;
 Meteor.call('connMongo', obj._id, obj, "upsert");
 console.log(obj);
 return Gamification.findOne(obj.id);
 
 
 //    var cont = 0;
 //   Gamification.find({}).forEach(function () {
 //  console.log(cont++);
 //  });
 
 }
 */
/*
 function createGamificationReferenceModels() {
 if (Gamification.findOne({"name": "Modelo-achievement"}) == null) {
 var achievementModelo = {name: "Modelo-achievement", leaderboard: true, personalStatus: false, score: true, achievementBoard: true,
 "scoreRules": {level: true, ratio: 0.2, levelDificulty: 0.3}};
 var socialModelo = {name: "Modelo-social", gameOn: true, score: true,
 "scoreRules": {level: true, ratio: 0.5, levelDificulty: 0.9, gameChance: 0.75}};
 var imersionModelo = {name: "Modelo-imersion", profileOn: true, avatar: true, background: true};
 var drawModelo = {name: "Modelo-draw", leaderboard: true, score: true, gameOn: true, avatar: true, background: true,
 "scoreRules": {level: true, ratio: 0.5, levelDificulty: 0.9, gameChance: 0.75}};
 
 Meteor.call('connMongo', achievementModelo.name, achievementModelo, "upsert");
 Meteor.call('connMongo', socialModelo.name, socialModelo, "upsert");
 Meteor.call('connMongo', imersionModelo.name, imersionModelo, "upsert");
 Meteor.call('connMongo', drawModelo.name, drawModelo, "upsert");
 }
 }
 */
/*
 function computeUserScore(user, tdfname) {
 var indivUserQuery = {'_id': user._id};
 // We use findOne because there should only ever be one user with any given id.
 var indivUser = UserMetrics.findOne(indivUserQuery);
 var askCount = 0;
 var correctCount = 0;
 
 _.chain(indivUser).prop(tdfname).each(function (item) {
 askCount += _.chain(item).prop('questionCount').intval().value();
 correctCount += _.chain(item).prop('correctAnswerCount').intval().value();
 });
 // var tryValues = getCurrentScoreValues();
 // console.log( "try calues "+Values[0]);
 return correctCount / askCount;  // Note the possible of DBZ which would return NaN
 
 // getCurrentTdfFile()
 
 }
 */

/*
 //Actual logic for selecting and starting a TDF
 function selectTdf(tdfkey, lessonName, stimulusfile, tdffilename, how) {
 //make sure session variables are cleared from previous tests
 sessionCleanUp();
 
 //Set the session variables we know
 //Note that we assume the root and current TDF names are the same.
 //The resume logic in the the card template will determine if the
 //current TDF should be changed due to an experimental condition
 Session.set("currentRootTdfName", tdffilename);
 Session.set("currentTdfName", tdffilename);
 Session.set("currentStimName", stimulusfile);
 
 //Get some basic info about the current user's environment
 var userAgent = "[Could not read user agent string]";
 var prefLang = "[N/A]";
 try {
 userAgent = _.display(navigator.userAgent);
 prefLang = _.display(navigator.language);
 } catch (err) {
 console.log("Error getting browser info", err);
 }
 
 //Save the test selection event
 recordUserTime("profile tdf selection", {
 target: lessonName,
 tdfkey: tdfkey,
 tdffilename: tdffilename,
 stimulusfile: stimulusfile,
 userAgent: userAgent,
 browserLanguage: prefLang,
 selectedHow: how
 });
 
 Router.go("/choose");
 }
 */
/*
 
 function GetUserAchievements(user, tdfname, unlocked) {
 var query = {};
 var orderby = {};
 orderby.sort = {};
 orderby.sort.tdf = -1;
 
 query.user_id = user._id;
 if (tdfname != null) {        
 query.tdf = buildTdfDBName(tdfname);
 }
 if (unlocked) {
 query.unlocked = unlocked;
 } else if (unlocked === false) {
 query.unlocked = unlocked;
 }
 
 if(unlocked) {
 orderby.sort.dateUnlocked = -1;   
 } 
 console.log(orderby);
 console.log(query);
 
 var userAchievements = Achievements.find(query,orderby).fetch();
 if (userAchievements != null) {
 return userAchievements;
 } else {
 return false;
 }
 
 }
 */
//function click(user) {
//    RegisterInteraction(user, "leaderBoard");
//
//}