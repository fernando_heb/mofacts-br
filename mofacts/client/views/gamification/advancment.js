


Template.advancment.helpers({
    showLeaderboard: function () {        
        return GetUserShowLeaderBoard(Meteor.user());
    },
    showStatus: function () {        
        return GetUserShowPersonalStatus(Meteor.user());
    },
    hasOption: function () {              
        return GetUserHasOption(Meteor.user());
    },
    user: function () {
        if (!haveMeteorUser()) {
            sessionRouting("advancments","signin");
            routeToSignin();
        } else {
            return Meteor.user();
        }
    },
});


////////////////////////////////////////////////////////////////////////////
// Template Events

Template.advancment.events({
    'change #optRanking': function () {
        changeOption(selectedRadio());
    },
    'change #optDesempenho': function () {
        changeOption(selectedRadio());
    },
    'click .btnf': function () {
        console.log("selected = " + selectedRadio());
    },
    'click #btnf': function () {
        console.log("teste2");
    }

});

function changeOption(selected) {
    console.log(selected);
    var obj ={};
   
   // var userModel = Gamification.findOne({_id: Meteor.user()._id});
    if (selected === 1) {
        //userModel.leaderboard = true;
        //userModel.personalStatus = false;
     obj = {'$set': {'leaderboard': true,'personalStatus':false }};
     RegisterInteraction(Meteor.user(),"leaderboard");
        //console.log("hello");        
    } else {
        obj = {'$set': {'leaderboard': false,'personalStatus':true }};
         RegisterInteraction(Meteor.user(),"personalStatus");
        //userModel.leaderboard = false;
       // userModel.personalStatus = true;        
    }
    console.log("change options");
    
                        
    Meteor.call("connGamification", {_id: Meteor.user()._id}, obj, "upsert");
    //console.log(userModel.scoreRules["level"]);
    //Meteor.call('connGamification',userModel._id, userModel, "update");
}

function selectedRadio() {
    var radios = document.getElementsByName('optradio');
    var value = null;
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            // do whatever you want with the checked radio
            value = radios[i].value;
            // only one radio can be logically checked, don't check the rest
            break;
        }
    }
    //console.log(value);
    if (value == "leader") {
        return 1;
    } else {
        return 0;
    }
    //return value;
}
