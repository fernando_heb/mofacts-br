

Template.stats.helpers({
    "user": function () {//HiraganaOptim_xml Fernando
        return ComposePerformanceTable(Meteor.user(), getCurrentTdfName());
    }

});

Template.stats.events({
    'click .personalStatus': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "personalStatus");
    },
});