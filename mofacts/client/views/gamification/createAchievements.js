Template.createAchievements.helpers({
    /** Achievement Collection fields
     * Achievment_id dateUnlocked description icoUrl operator  tdf title unlocked
     user_id value variable _id
     */
    'IsSocializer': function () {
        if (GetUserProfileName(Meteor.user()) == "Socializer") {
            return true;
        } else {
            return false;
        }
    },
    'achievmentsTrue': function () {
        return GetUserAchievements(Meteor.user(), null, true);
    },
    'achievmentsFalse': function () {
        return GetUserAchievements(Meteor.user(), null, false);
    },
    username: function () {
        if (!haveMeteorUser()) {
            sessionRouting("createachievements","signin");
            routeToSignin();
        } else {
            return Meteor.user().username;
        }
    },
    'DesafioText': function () {
        var perfil = GetUserProfileName(Meteor.user());
        var texts = {};
        switch (perfil) {
            case "Socializer":
                texts.header = "Crie uma Nova Sugestão de Meta";
                texts.placeHolderTitle = "Nomeie sua Meta";
                texts.placeHolderDescription = "Descreva sua meta";
                texts.placeHolderValue = "Quanto será necessário";
                texts.button = "Sugerir para todos";
                texts.new = "Legal! Voce têm uma nova meta sugerida!";
                break;
            case "Achiever":
                texts.header = "Crie um Novo Desafio";
                texts.placeHolderTitle = "Dê um Título ao seu Desafio";
                texts.placeHolderDescription = "Descreva seu desafio";
                texts.placeHolderValue = "Quanto será necessário";
                texts.button = "Lançar seu desafio";
                break;
            default:
                texts.header = "Crie um Novo Desafio";
                texts.placeHolderTitle = "Dê um Título ao seu Desafio";
                texts.placeHolderDescription = "Descreva seu desafio";
                texts.placeHolderValue = "Quanto será necessário";
                texts.button = "Lançar seu desafio";
        }
        texts.variable = "De que será a quantidade?";
        texts.operator = "Condição";
        texts.lesson = "Para qual lição você está criando";
        texts.custoIco = "$$";
        texts.custo = "3000";
        return texts;
    },
    'operators': function () {
        var options = [
            {value: ">", description: "Maior que"},
            {value: "=", description: "Igual a"},
            {value: "<", description: "Menor que"}
        ];
        return options;
    },
    'variables': function () {
        var options = [
            {value: "points", description: "Total de Pontos"},
            {value: "acertos", description: "Total de acertos"},
            {value: "AvgAcertos", description: "Média de acertos (%)"},
            {value: "latency", description: "Tempo de resposta (ms)"},
            {value: "AvgLatency", description: "Tempo médio de resposta (ms)"},
            {value: "level", description: "Nível da disciplina"},
            {value: "TotalLevel", description: "Nível total"},
            {value: "TotalPoints", description: "Total de Pontos"}];
        return options;
    },
    'tdfsOptions': function () {
        var options = [];
        var tdfsAvailable = [];
        tdfsAvailable = userAnsweredTDFS(Meteor.user());

        tdfsAvailable.forEach(function (tdf) {
            var originalName = tdf.replace("_", ".");
            var tdfs = Tdfs.findOne({fileName: originalName});
            if (tdfs != null) {
                var info = {};
                info.lessonname = tdfs.tdfs.tutor.setspec[0].lessonname[0];
                info.filename = buildTdfDBName(tdfs.fileName);
                options.push(info);
            }
        });
        return options;
    },
});
////////////////////////////////////////////////////////////////////////////
// Template Events

Template.createAchievements.events({
    'click .gamification': function (event) {
        event.preventDefault();
        sessionRouting("createachievements","gamification");
        Router.go('/gamification');
    },
//    'change #lessonSelector': function () {
//        // $("#tdfSelector").change(function(){
//       tdf = $("#lessonSelector").find(':selected').val();
//       lessonname = $("#lessonSelector").find(':selected').attr("lesson");
//       
//    },
//    'change #variableSelector': function () {
//        // $("#tdfSelector").change(function(){
//        variable = $("#variableSelector").find(':selected').val();
//        
//       
//    },
//    'change #operatorSelector': function () {
//        // $("#tdfSelector").change(function(){
//        operator = $("#operatorSelector").find(':selected').val();
//        
//         
//       
//       
//    },
    'click .createAchievement': function (event) {
        event.preventDefault();
        var obj = {};
        obj.title = $("#title").val();
        obj.description = $("#description").val();
        obj.value = $("#value").val();
     //   $('input[name="tdf"]:checked').val();
    //    obj.createdBy = userGetUsername(Meteor.user());
      //  obj.tdf = $('input[name="tdf"]:checked').attr("id");
        obj.tdf = $("#lessonSelector").find(':selected').val();
//        obj.lessonname = $('input[name="tdf"]:checked').val();
        obj.lessonname = $("#lessonSelector").find(':selected').attr("lesson");

        //obj.unlocked = false;
//        obj.variable = $('input[name="variable"]:checked').val();
        obj.variable = $("#variableSelector").find(':selected').val();
//        obj.operator = $('input[name="operators"]:checked').val();
        obj.operator = $("#operatorSelector").find(':selected').val();
        //      obj.icoUrl = null;
        obj.createdBy = userGetShortname(Meteor.user());
        if (obj.createdBy.toLowerCase() == "fernando.heb")
            obj.createdBy = "GameMaster";

        if (verificaObj(obj)) {

            var custo = 3000    //GetUserLevel(null,obj.tdf) * 50;
            var lowPointsMsg = "Humm.. Que Pena você não tem pontos suficientes para isso no momento";
            if (ExpendPoints(null, custo, null, lowPointsMsg) === false)
                return false;

            Meteor.call('connAchievementModels', null, obj, "insert");
            verifyUserAchievementModel(Meteor.user(), obj.tdf);
            alert("Gravação concluida sua mensagem será levada a todos!");

            //ReduceUserScore(null,obj.tdf,custo); //colocar informação do custo na tela 
            //RegisterInteraction(Meteor.user(), "ExpendedPoints");    
            
            sessionRouting("createAchievements", "achievementboard");          
            Router.go('/AchievementBoard');
        } else {
            alert("Existem campos em branco ou você não colocou um valor válido");
        }
        RegisterInteraction(Meteor.user(), "AchievementsCreation");

    }

});
function verificaObj(obj) {
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            if ((obj[property] == null) || (obj[property] == "") || (obj[property] == 0)) {
                return false;
            }
        }

    }
    return true;
}


