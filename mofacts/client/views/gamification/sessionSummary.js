////////////////////////////////////////////////////////////////////////////
// Local collection for buttons

////////////////////////////////////////////////////////////////////////////
// Template storage and helpers
Template.sessionSummary.helpers({
    username: function () {
        if (!haveMeteorUser()) {
            sessionRouting("sessionsummary", "signin");
            routeToSignin();
        } else {
            return Meteor.user().username;
        }
    },
    'sessionInfo': function () {
        var session = GetUserSession(Session.get("SessionID"));
        if (session == null) {
            session = {};
        }
        session.totalWrong = 0;
        session.percRight = 0;
        session.percWrong = 0;
        session.duration = '00:00:00';
        if (session.hasOwnProperty("totalQuestions")) {
            if (session.totalQuestions > 0) {
                session.totalWrong = session.totalQuestions - session.totalCorrect;
                session.percRight = Math.round((session.totalCorrect / session.totalQuestions) * 100);
                session.percWrong = Math.round((100 - session.percRight) * 100);
            }
        } else {
            session.totalQuestions = 0;
            session.totalCorrect = 0;
        }
        var durate = new Date;
        if (session.hasOwnProperty('finished')) {
            durate = session.finished.getTime() - session.start.getTime();
//           session.duration = Math.floor(durate / 60000);       
        } else if (session.hasOwnProperty('lastseen')) {
            durate = session.lastseen.getTime() - session.start.getTime();
//           session.duration = Math.floor(durate / 60000);
//           durate  = session.lastseen - session.start;          
//            session.duration = durate.getMinutes();        
        }
        if (durate > 0) {
            var diffMs = (durate); // milliseconds between now & Christmas
            var diffDays = Math.floor(diffMs / 86400000) > 0 ? Math.floor(diffMs / 86400000) : "00";  // days
            var diffHrs = Math.floor((diffMs % 86400000) / 3600000) > 0 ? Math.floor((diffMs % 86400000) / 3600000) : "00"; // hours
            var diffMins = Math.floor(((diffMs % 86400000) % 3600000) / 60000) > 0 ? Math.floor(((diffMs % 86400000) % 3600000) / 60000) : 00; // minutes
            var diffsecs = Math.floor(((diffMs % 86400000) % 3600000) % 60000 / 1000) > 0 ? Math.floor(((diffMs % 86400000) % 3600000) % 60000 / 1000) : 00; // minutes
            session.duration = diffHrs + ":" + diffMins + ":" + diffsecs;
        }


        return session;
    },
    'controle': function () {
    },
    'randImg': function () {
        return RollDice(11);
    }


});

////////////////////////////////////////////////////////////////////////////
// Template Events

Template.sessionSummary.events({
    'click .logoutLink': function (event) {
        event.preventDefault();
        var happiness = $('input[name="likertHappiness"]:checked').val();
        var control = $('input[name="likertControl"]:checked').val();
        var arousal = $('input[name="likertArousal"]:checked').val();
        var comment = $('textarea#comment').val();
        FinishSession(Meteor.user(), happiness, control, arousal, comment);
        Meteor.logout(function (error) {
            if (typeof error !== "undefined") {
                //something happened during logout
                console.log("User:", Meteor.user(), "Error:", error);
            } else {
                sessionRouting("sessionsummary", "signin");
                routeToSignin();
            }
        });
        //  routeToSummaryLogout();
    },
    'click .test': function (event) {
        console.log($('input[name="likertControle"]:checked').val());
    }

});


Template.sessionSummary.rendered = function () {
    //this is called whenever the template is rendered.


};
function click(user) {
    RegisterInteraction(user, "summary");

}
