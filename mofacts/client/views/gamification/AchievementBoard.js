Template.AchievementBoard.helpers({
    /** Achievement Collection fields
     * Achievment_id dateUnlocked description icoUrl operator  tdf title unlocked
     user_id value variable _id
     */
    'IsSocializer': function () {
        if (GetUserProfileName(Meteor.user()) == "Socializer") {
            return true;
        } else {
            return false;
        }
    },
    'achievementsUnlocked': function () {
        return GetUserAchievements(Meteor.user(), null, true);
    },
    'achievementsLocked': function () {
        return GetUserAchievements(Meteor.user(), null, false);
    },
    username: function () {
        if (!haveMeteorUser()) {
            sessionRouting("achievementboard","signin");
            routeToSignin();
        } else {
            return Meteor.user().username;
        }
    },
    ShowCreateAchievment : function (){
        return GetUserShowCallChalange(Meteor.user());
        
    }
    ,
    'DesafioText': function () {
        var perfil = GetUserProfileName(Meteor.user());
        var texts = {};       
        switch (perfil) {
            case "Socializer": //Friendly Design
                texts.kind = "socializer";
                texts.btnSuggest = "Troque seus pontos e Lance Sua Propria Meta";
                texts.aberto = "Metas em aberto";
                texts.closed = "Metas alcançadas";
                texts.suggested = "Sugerido por: ";
                texts.suggestedDone = "Você cumpriu a meta de: ";
                texts.new = "Legal! Voce têm uma nova meta sugerida!";                
                texts.social = true;
                break;
            case "Achiever": //Competitivo
                texts.kind = "achiever";
                texts.btnSuggest = "Troque seus pontos e Lance um Desafio para Todos!";
                texts.aberto = "Desafios bloqueado";
                texts.closed = "Desafios concluidos";
                texts.suggested = "Você foi desafiado por: ";
                texts.suggestedDone = "Você derrotou: ";
                texts.new = "Hey! Voce recebeu um novo desafio!";
                break;
            default: //Imersivo, Customização
                texts.kind = "default";
                texts.btnSuggest = "Troque seus pontos e Lance um Desafio para Todos!";
                texts.aberto = "Desafios bloqueado";
                texts.closed = "Desafios concluidos";
                texts.suggested = "Você foi desafiado por: ";
                texts.suggestedDone = "Você derrotou: ";
                texts.new = "Hey! Voce recebeu um novo desafio!";
                break;
        }
        return texts;
    },
});



////////////////////////////////////////////////////////////////////////////
// Template Events

Template.AchievementBoard.events({
    'click .gamification': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "GameProfile");
        sessionRouting("achievementboard","gamification");
        Router.go('/gamification');
    },
    'click .createAchievement': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "AchievementsCreation");
        sessionRouting("achievementboard","createachievements");
        Router.go("/createAchievements");
    },
    'click .socializer': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "Achievements");       
    },
    'click .achiever': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "Achievements");       
    },
    'click .default': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "Achievements");       
    }
    
    
    
    
    
    
   
});

