Template.choose.helpers({
    username: function () {
        if (!haveMeteorUser()) {
            sessionRouting("choose","signin");
            routeToSignin();
        }
        else {
            return Meteor.user().username;
        }
    }
});

Template.choose.events({
/*	 'click .logoutLink' : function (event) {
        event.preventDefault();
        Meteor.logout( function (error) {
            if (typeof error !== "undefined") {
                //something happened during logout
                console.log("User:", Meteor.user(), "Error:", error);
            }
            else {
                routeToSignin();
            }
        });
    },

    'click .homeLink' : function (event) {
        event.preventDefault();
        Router.go("/profile");
    },

    'click .allItemsLink' : function (event) {
      event.preventDefault();
      Router.go("/allItems");
   },

    'click .adminLink' : function (event) {
        event.preventDefault();
        Router.go("/admin");
    },*/

    'click .studentButton' : function (event) {
    	event.preventDefault();
        sessionRouting("choose","allstudents");
    	Router.go("/allStudents");
    },

    'click .itemsButton' : function (event) {
    	event.preventDefault();
        sessionRouting("choose","items");
    	Router.go("/Items");
   }

});
