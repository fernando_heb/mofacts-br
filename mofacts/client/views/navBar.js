////////////////////////////////////////////////////////////////////////////
// Template storage and helpers

//INPUT: user, which is an object containing an _id which corresponds to a doc
//       in UserMetrics, and the name of the relevant Tdf (in Mongo-recognizable
//       format)
//OUTPUT: a ratio which is the user's average score across all items for the
//        client's current system.

Template.navBar.helpers({
    username: function () {
        if (!haveMeteorUser()) {
            //routeToSignin();
              routeToSummary();
        } else {
            return Meteor.user().username;
        }
    },
    'isNormal': function () {
        return Session.get("loginMode") !== "experiment";
    },
    showLevel: function () {
        return GetUserShowLevel(Meteor.user());
    },
    showAchievments: function () {
        return GetUserShowAchievementBoard(Meteor.user());
    },
    level: function () {        
        return GetUserTotalLevel(Meteor.user(), null);;
    },
    showScore: function () {
        return GetUserShowScore(Meteor.user());
    },
    showProfile: function () {
        return GetUserShowProfile(Meteor.user());
    },
    totalPoints: function () {
        return GetUserTotalPoints(Meteor.user());
    },
    NewAchievmentUnlocked : function() {
        var bool = GetAchievementNotificationBody(Meteor.user());
        if(bool){
           return "NewAchievement"; 
        }  else {
            return "hide";
        }
    },
    AchievementNotificationBody : function(){        
        return GetAchievementNotificationBody(Meteor.user());         
    },
     'IsSocializer' : function(){
      if(GetUserProfileName(meteor.user() == "Socializer")) {
          return true;
      } else {
          return false;
      }
    },
     'PerfisTexts': function () {
    var perfil = GetUserProfileName(Meteor.user());
        var texts = {};
        switch (perfil) {
            case "Socializer": //Friendly Design
                texts.kind = "socializer";
                texts.BtnAchievementBoard = "Minhas Metas";
                texts.social = true;
                texts.ico = "fa-flag";
                break;
            case "Achiever": //Competitivo
                texts.kind = "achiever";
                texts.BtnAchievementBoard = "Minhas conquistas";
                texts.new = "Hey! Voce recebeu um novo desafio!";
                texts.ico = "fa-trophy";
                break;
            default: //Imersivo, Customização
                texts.kind = "default";               
                texts.BtnAchievementBoard = "Desafios";
                texts.new = "Hey! Voce recebeu um novo desafio!";
                texts.ico = "fa-flag";
                break;
        }
        return texts;
        },
        'gamified':function (){
            return GetUserModel(Meteor.user());
        }
        

});


////////////////////////////////////////////////////////////////////////////
// Template Events

Template.navBar.events({
    'click .logoutLink': function (event) {
      //  FinishSession(Meteor.user());
        updateSessionInfo(null,null);
        event.preventDefault();
        sessionRouting("navbar","signout");
        routeToSummaryLogout();
//        Meteor.logout(function (error) {
//            if (typeof error !== "undefined") {
//                //something happened during logout
//                console.log("User:" + Meteor.user() + " ERROR:" + error);
//            } else {
//               routeToSignin();
//               
//            }
//        });
    },
    'click .homeLink': function (event) {
        event.preventDefault();
        sessionRouting("navbar","profile");
        Router.go("/profile");
    },
    'click .studyLink': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "Study");
        sessionRouting("navbar","study");
        Router.go("/study");
    },
    'click .achievmentBoard': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "achievementBoard");
        RemoveAchievmentNotification(Meteor.user());  
        sessionRouting("navbar","achievmentboard");
        Router.go("/AchievementBoard");
    },
    
     'click .gamification': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(), "GameProfile");
        sessionRouting("navbar","gamification");
        Router.go('/gamification');
    },
    
    
    'click .allItemsLink': function (event) {
        event.preventDefault();
        sessionRouting("navbar","allitems");
        Router.go("/allItems");
    },
    'click .adminLink': function (event) {
        event.preventDefault();
        sessionRouting("navbar","admin");
        Router.go("/admin");
    },
    //Sets the session variable for the student that is selected
    //along with setting the username for display on the graph legend
    'click .studentButton': function (event) {
        var target = $(event.currentTarget);
        Session.set('currStudent', event.target.id);
        Session.set('currUsername', event.target.value);
        event.preventDefault();
        sessionRouting("navbar","student");
        Router.go('/student');
    },
    'click .levelTotal': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(),"levelTotal");
    },
    'click .pointsTotal': function (event) {
        event.preventDefault();
        RegisterInteraction(Meteor.user(),"pointsTotal");
    },
   
});


//*************************************************************//
/*
 Template.navBarCard.helpers({
 //    username: function () {
 //        if (!haveMeteorUser()) {
 //             leavePage(routeToSignin());
 //        }
 //        else {
 //            return Meteor.user().username;
 //        }
 //    },
 
 'currentScore': function() {
 //return Session.get("currentScore");
 
 return GetUserPoints(Meteor.user(),getCurrentTdfName());
 },
 
 showScore: function () {
 //var userModel = Gamification.findOne({_id: Meteor.user()._id}, {_id: 0, score: 1});
 
 var userModel = GetUserModel(Meteor.user());
 console.log(userModel);
 return userModel.score;
 },
 
 showLevel: function () {
 var userModel = GetUserModel(Meteor.user());
 //       console.log("NavbarCard usermodel");
 //       console.log(userModel);
 //        console.log("NavbarCard usermodel scoreRules Level");
 //        console.log(userModel.scoreRules.level);
 return userModel.scoreRules.level;
 },
 
 level: function () {
 var level =  GetUserLevel(Meteor.user(), getCurrentTdfName());
 console.log("Navbarcard level: ");
 console.log(level);
 return level;
 }
 });
 
 
 ////////////////////////////////////////////////////////////////////////////
 // Template Events
 
 Template.navBarCard.events({
 /*'click .logoutLink' : function (event) {
 Meteor.logout( function (error) {
 event.preventDefault();
 if (typeof error !== "undefined") {
 //something happened during logout
 console.log("User:" + Meteor.user() +" ERROR:" + error);
 }
 else {
 leavePage(routeToSignin);
 }
 });
 },
 'click .allItemsLink' : function (event) {
 event.preventDefault();
 leavePage("/allItems");
 },
 
 'click .homeLink' : function (event) {
 event.preventDefault();
 leavePage("/profile");
 },
 
 'click .adminLink' : function (event) {
 event.preventDefault();
 leavePage("/admin");
 },
 'currentScore': function() {
 return Session.get("currentScore");
 },
 'isNormal': function() {
 return Session.get("loginMode") !== "experiment";
 },
 //Sets the session variable for the student that is selected
 //along with setting the username for display on the graph legend
 //  /*  'click .studentButton' : function (event) {
 //       var target = $(event.currentTarget);
 //      Session.set('currStudent', event.target.id);
 //     Session.set('currUsername', event.target.value);
 //    event.preventDefault();
 //    leavePage('/student');
 }
 });*/



